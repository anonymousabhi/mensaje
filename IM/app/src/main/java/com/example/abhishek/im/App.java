package com.example.abhishek.im;

import android.app.Application;
import android.content.Context;

import com.example.smacklibrary.MyApp;

/**
 * Created by abhishek on 7/6/16.
 */
public class App extends MyApp {
    private static Context mContext;

    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }
}
