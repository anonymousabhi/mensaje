package com.example.abhishek.im;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by abhishek on 12/7/16.
 */
public class Request_Adapter extends RecyclerView.Adapter<Request_Adapter.MyViewHolder> {
    private LayoutInflater inflater;
    ChatUtilityApp mchatUtilityApp;
    List<String> users = Collections.emptyList();
    FrontPage_Activity frontPage_activity;

    public Request_Adapter(Context context, List<String> users, FrontPage_Activity frontPage_activity) {
        mchatUtilityApp = ChatUtilityApp.getInstance(context);
        inflater = LayoutInflater.from(context);
        this.users = users;
        this.frontPage_activity = frontPage_activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String current_User = users.get(position);
        holder.invite_textView.setText(current_User);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView invite_textView;
        Button bt_yes, bt_no;

        public MyViewHolder(View itemView) {
            super(itemView);
            invite_textView = (TextView) itemView.findViewById(R.id.tv_invite_from);
            bt_yes = (Button) itemView.findViewById(R.id.bt_yes);
            bt_yes.setOnClickListener(this);
            bt_no = (Button) itemView.findViewById(R.id.bt_no);
            bt_no.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                String current_User = users.get(getAdapterPosition());

                if (v.getId() == R.id.bt_yes) {
                    clickListener.itemclick(current_User, true, getAdapterPosition());
                } else {
                    clickListener.itemclick(current_User, false, getAdapterPosition());
                }

            }

            //ChatThread thread = new ChatThread(User.get_id(current_User), User.get_id(current_User));
            //thread.setCurrentThreadId(User.get_id(current_User));

        }
    }

    private ClickListener clickListener;

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        public void itemclick(String user,boolean isAccept,int position);
    }
}
