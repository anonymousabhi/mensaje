package com.example.abhishek.im;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.example.smacklibrary.CallBack;
import com.example.smacklibrary.SmackService;
import com.example.smacklibrary.StorageUtil;
import com.example.smacklibrary.User;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    StorageUtil storageUtil;
    EditText editText_fullname, editText_email, editText_password;
    Button button_login;
    TextInputLayout ErrorName, ErrorPassword, ErrorEmail;
    CallBack mCallBack;
    Toolbar toolbar;
    CheckBox checkBox;
    Progress progress = new Progress(this);
    CallBack.CallbackMethods mCallbackMethods = new CallBack.CallbackMethods() {

        @Override
        public void newActivity() {
            Intent intent = new Intent(new Intent(MainActivity.this, FrontPage_Activity.class));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            progress.stop();
            startActivity(intent);
        }

    };

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void generateError(TextInputLayout t, String Message) {
        t.setErrorEnabled(true);
        t.setError(Message);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        storageUtil = new StorageUtil(this);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        User user = storageUtil.getUser();
        if (user != null) {
            int mUserstatus = storageUtil.getUserStatus();
            if (mUserstatus == 1) {
                startActivity(new Intent(this, FrontPage_Activity.class));
            }
        }
        mCallBack = CallBack.getInstance(this);
        mCallBack.register_Callback(mCallbackMethods);
        editText_email = (EditText) findViewById(R.id.edittext_email);
        editText_password = (EditText) findViewById(R.id.edittext_password);
        editText_fullname = (EditText) findViewById(R.id.edittext_name);
        button_login = (Button) findViewById(R.id.button_login);
        ErrorEmail = (TextInputLayout) editText_email.getParent();
        ErrorPassword = (TextInputLayout) editText_password.getParent();
        ErrorName = (TextInputLayout) editText_fullname.getParent();
        button_login.setOnClickListener(this);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    editText_password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                } else {
                    editText_password.setInputType(129);
                }

            }
        });
        // initConnection();
    }


    @Override
    public void onClick(View v) {
        int count = 0;
        String name, email, password;
        name = editText_fullname.getText().toString();
        email = editText_email.getText().toString();
        password = editText_password.getText().toString();
        if (!isValidEmail(email)) {
            generateError(ErrorEmail, "Enter Valid Email");
        } else {
            count++;
            if (password.length() < 8) {
                generateError(ErrorPassword, "Min 8 Characters");
            } else {
                count++;
                if (name.length() < 4) {
                    generateError(ErrorName, "Min 4 Characters");
                } else {
                    count++;
                }
            }
        }
        if (count == 3) {
            Intent intent = new Intent(MainActivity.this, SmackService.class);
            intent.putExtra("email", email);
            intent.putExtra("password", password);
            intent.putExtra("name", name);
            intent.putExtra("activity", "MainActivity");
            progress.show();
            startService(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
    }
}