package com.example.abhishek.im;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.abhishek.im.Fragments.Chats;
import com.example.abhishek.im.Fragments.Invite;
import com.example.abhishek.im.Fragments.Request;
import com.example.smacklibrary.MyAIDL;
import com.example.smacklibrary.SmackService;
import com.example.smacklibrary.StorageUtil;
import com.example.smacklibrary.User;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class FrontPage_Activity extends AppCompatActivity implements MaterialTabListener {
    ViewPager mPager;
    StorageUtil storageUtil;
    User user;
    MaterialTabHost tabHost;
    Toolbar toolbar;
    MyPageAdapter myPageAdapter;
    MyAIDL myAIDL = null;
    ServiceConnection mConnection;


    @Override
    protected void onResume() {
        super.onResume();
        for (int i = 0; i < myPageAdapter.getCount(); i++) {
            tabHost.addTab(tabHost.newTab().setText(myPageAdapter.getPageTitle(i)).setTabListener(this));
        }
    }

    private void init() {

        mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                myAIDL = MyAIDL.Stub.asInterface(service);
                //  Toast.makeText(FrontPage_Activity.this, "onServiceConnected", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                myAIDL = null;
                //Toast.makeText(FrontPage_Activity.this, "onServiceDisconnected", Toast.LENGTH_SHORT).show();
            }
        };
        if (myAIDL == null) {
            Intent intent = new Intent(FrontPage_Activity.this, SmackService.class);
            intent.setAction("invite");
            bindService(intent, mConnection, Service.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindService(mConnection);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_page_);
        storageUtil = new StorageUtil(this);
        storageUtil.setUserStatus(1);
        user = storageUtil.getUser();
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (user != null)
            actionBar.setTitle("Welcome " + user.getName());
        mPager = (ViewPager) findViewById(R.id.pager);
        tabHost = (MaterialTabHost) findViewById(R.id.materialTabHost);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                tabHost.setSelectedNavigationItem(position);
            }
        });
        myPageAdapter = new MyPageAdapter(getSupportFragmentManager());
        mPager.setAdapter(myPageAdapter);

        /*for (int i = 0; i < myPageAdapter.getCount(); i++) {
            tabHost.addTab(tabHost.newTab().setText(myPageAdapter.getPageTitle(i)).setTabListener(this));
        }*/
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                } else if (position == 1) {

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        init();
    }

    public void InviteStatus(String current_User, boolean isAdd) {
        try {
            myAIDL.isAccepted(current_User, isAdd);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void sendInvite(String invite_email, Boolean isAdd) {
        try {
            myAIDL.onAddUser(invite_email, isAdd);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTabSelected(MaterialTab tab) {
        mPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab tab) {

    }

    @Override
    public void onTabUnselected(MaterialTab tab) {

    }

    private Fragment setFragment(int number) {
        if (number == 0) {
            Chats chats = new Chats();
            return chats;
        } else if (number == 1) {
            Request frag = new Request();
            return frag;
        } else {
            Invite frag = new Invite();
            return frag;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.front_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.roster_list:
                startActivity(new Intent(this, Roster_List.class));
                break;
            case R.id.logout:
                storageUtil.clear();
                startActivity(new Intent(this, MainActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
    }

    class MyPageAdapter extends FragmentPagerAdapter {
        String tabs[] = getResources().getStringArray(R.array.tabs);

        public MyPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabs[position];
        }

        @Override
        public Fragment getItem(int position) {
            return setFragment(position);
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
