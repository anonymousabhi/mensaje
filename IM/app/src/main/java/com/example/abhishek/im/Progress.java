package com.example.abhishek.im;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by abhishek on 31/7/16.
 */
public class Progress {
    ProgressDialog Dialog;
    Context context;
    boolean ifstarted;

    public Progress(Context c) {

        ifstarted = false;
        context = c;
    }

    public void show() {

        if (!ifstarted) {
            Dialog = new ProgressDialog(context);
            Dialog.setTitle("Signing In...");
            Dialog.setMessage("Please Wait...");
            Dialog.show();
            ifstarted = true;
        }

    }

    public void stop() {

        if (ifstarted) {
            Dialog.cancel();
        }

        ifstarted = false;

    }
}
