package com.example.abhishek.im;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.smacklibrary.User;

import java.util.Collections;
import java.util.List;

/**
 * Created by abhishek on 14/7/16.
 */
public class Chat_Adapter extends RecyclerView.Adapter<Chat_Adapter.ChatViewHolder> {
    private LayoutInflater inflater;
    List<String> userList = Collections.emptyList();
    Context mContext;

    public Chat_Adapter(Context context, List<String> userList) {
        inflater = LayoutInflater.from(context);
        this.userList = userList;
        mContext = context;
    }

    @Override
    public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_chatrow, parent, false);
        ChatViewHolder holder = new ChatViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ChatViewHolder holder, int position) {
        String current_User = userList.get(position);
        holder.user_textview.setText(current_User);
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    class ChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView user_textview;
        CardView cardView;

        public ChatViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
            user_textview = (TextView) itemView.findViewById(R.id.tv_user);
            user_textview.setOnClickListener(this);
            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            String peer = user_textview.getText().toString();
            if (peer != null && !peer.equals("No chat thread present")) {
                peer = User.get_id(peer);
                Intent intent = new Intent(mContext, ChattingActivity.class);
                intent.putExtra("peer", peer);
                intent.putExtra("activity", "front");
                mContext.startActivity(intent);
            }
        }
    }
}
