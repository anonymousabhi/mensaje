package com.example.abhishek.im;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.Html;
import android.util.Log;

import com.example.smacklibrary.ChatMessage;
import com.example.smacklibrary.ChatMessagesDaoImpl;
import com.example.smacklibrary.ChatThread;
import com.example.smacklibrary.ChatThreadsDaoImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


public class ChatUtilityApp {
    private static final String TAG = ChatUtilityApp.class.getSimpleName();

    private static final int CHAT_NOTIFICATION_ID = 1001;
    private static final int MAX_ALLOWED_LINES = 7;

    private static ChatUtilityApp mInstance;
    private Context mContext;
    private NotificationManager mNotificationManager;
    private Bitmap mDefaultLargeIconBitmap;
    private NotificationCompat.Builder mChatNotificationBuilder;
    private HashMap<String, ChatThread> mChatThreads = new HashMap<>();


    private ChatUtilityApp(Context context) {
        mContext = context.getApplicationContext();

        mNotificationManager = (NotificationManager) mContext.getSystemService(Context
                .NOTIFICATION_SERVICE);

        mDefaultLargeIconBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable
                .ic_reply_black_24dp);
        mChatNotificationBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_reply_black_24dp)
                .setLargeIcon(mDefaultLargeIconBitmap)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setColor(context.getResources().getColor(R.color.colorAccent));
    }

    public static ChatUtilityApp getInstance(@NonNull Context context) {
        if (mInstance == null) {
            mInstance = new ChatUtilityApp(context);
        }

        return mInstance;
    }


    private void saveNewThreadId(String remoteParty, String threadId) {
        ChatThreadsDaoImpl.getInstance().setNewThreadId(mContext, remoteParty, threadId);
    }


    public HashMap<String, ArrayList<ChatMessage>> getUnreadMessages() {
        HashMap<String, ArrayList<ChatMessage>> unreadThreads = null;
        ChatMessage message = ChatMessagesDaoImpl.getInstance().getNewMessage(
                mContext);
        if (message != null) {
            unreadThreads = new HashMap<>();
            if (unreadThreads.containsKey(message.getRemoteParty())) {
                ArrayList<ChatMessage> linkedChats = unreadThreads.get(message.
                        getRemoteParty());
                linkedChats.add(message);
            } else {
                ArrayList<ChatMessage> linkedChats = new ArrayList<>();
                linkedChats.add(message);
                unreadThreads.put(message.getRemoteParty(), linkedChats);
            }
        }

        return unreadThreads;
    }

    private HashMap<String, ArrayList<ChatMessage>> prepareNotificationForUnreadMessages() {
        HashMap<String, ArrayList<ChatMessage>> unreadThreads = getUnreadMessages();
        if (unreadThreads != null && unreadThreads.size() > 0) {
            if (unreadThreads.size() == 1) {
                ArrayList<String> keys = new ArrayList<>(unreadThreads.keySet());
                ChatThread thread = getUserFromLocalDb(keys.get(0));
                ArrayList<ChatMessage> linkedMessages = unreadThreads.get(thread.getFrom());

                if (linkedMessages.size() == 1) {
                    ChatMessage message = linkedMessages.get(0);
                    CharSequence title = null, contentText = null;
                    if (message.getType() == ChatMessage.Type.TEXT) {
                        title = thread.getFromName();
                        contentText = message.getMessage();
                    } /*else {
                        title = message.getTypeThreadMessage(mContext);

                        /*boolean isRemoteMerchant = false; //using this only for offer expiry.
                        // in other cases it will throw wrong results, so don't rely on it.
                        String isMerchant = message.getJsonMessage().getExpiredByMerchant();
                        if (!TextUtils.isEmpty(isMerchant) && isMerchant.equals("1")) {
                            isRemoteMerchant = true;
                        }

                        String orderId = message.getJsonMessage().getOrderId();
                        contentText = getRosterNotificationText(message, thread.getFromName(),
                                isRemoteMerchant, orderId);
                    }*/

                    mChatNotificationBuilder.setContentTitle(title);
                    mChatNotificationBuilder.setContentText(contentText);
                    mChatNotificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(contentText));
                } else {
                    int linesAdded = 0;

                    NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();
                    mChatNotificationBuilder.setContentTitle(thread.getFromName());
                    mChatNotificationBuilder.setStyle(style);
                    mChatNotificationBuilder.setContentText(linkedMessages.size() + " new " +
                            "messages");
                    style.setSummaryText(linkedMessages.size() + " new messages");
                    for (ChatMessage message : linkedMessages) {
                        if (linesAdded == MAX_ALLOWED_LINES) {
                            break;
                        }

                        if (message.getType() == ChatMessage.Type.TEXT) {
                            style.addLine(message.getMessage());
                        } /*else {
                            boolean isRemoteMerchant = false; //using this only for offer expiry.
                            // in other cases it will throw wrong results, so don't rely on it.
                            String isMerchant = message.getJsonMessage().getExpiredByMerchant();
                            if (!TextUtils.isEmpty(isMerchant) && isMerchant.equals("1")) {
                                isRemoteMerchant = true;
                            }

                            String orderId = message.getJsonMessage().getOrderId();
                            CharSequence notificationText = getRosterNotificationText(message,
                                    thread.getFromName(), isRemoteMerchant, orderId);

                            style.addLine(notificationText);
                        }*/

                        linesAdded++;
                    }
                }
            } else {
                int mCount = 0, linesAdded = 0;
                mChatNotificationBuilder.setContentTitle(mContext.getString(R.string.app_name));

                NotificationCompat.InboxStyle style = new NotificationCompat.InboxStyle();
                mChatNotificationBuilder.setStyle(style);

                Set<String> keys = unreadThreads.keySet();
                for (String key : keys) {
                    ChatThread thread = getUserFromLocalDb(key);
                    ArrayList<ChatMessage> linkedChats = unreadThreads.get(key);
                    for (ChatMessage chat : linkedChats) {
                        if (linesAdded < MAX_ALLOWED_LINES) {
                            CharSequence msgTxt = Html.fromHtml("<b>" + thread.getFromName() +
                                    ":</b> " + (chat.getType() == ChatMessage.Type.TEXT ? chat
                                    .getMessage() : chat.getTypeThreadMessage(mContext)));
                            style.addLine(msgTxt);
                        }
                        linesAdded++;
                    }

                    mCount += linkedChats.size();
                }

                style.setSummaryText(mCount + " messages from " + unreadThreads.size() + " chats");
                mChatNotificationBuilder.setContentText(mCount + " messages from " +
                        unreadThreads.size() + " chats");
            }
        }

        return unreadThreads;
    }

    private synchronized void issueChatNotification() {
        Log.i(TAG, "issue Notification");
        HashMap<String, ArrayList<ChatMessage>> threads = prepareNotificationForUnreadMessages();
        if (threads == null || threads.size() == 0) {
            cancelRosterNotification();
            return;
        }
        Intent activityIntent;
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);

        if (mChatNotificationBuilder.mActions != null) {
            mChatNotificationBuilder.mActions.clear();
        }

        if (threads.size() == 1) {
            ArrayList<String> keys = new ArrayList<>(threads.keySet());
            ChatThread thread = getUserFromLocalDb(keys.get(0));

            if (thread != null) {
                ArrayList<ChatMessage> messages = threads.get(thread.getFrom());
                if (messages != null && messages.size() == 1) {
                    Intent laterIntent = new Intent(mContext, FrontPage_Activity.class);
                    laterIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    TaskStackBuilder acceptStackBuilder = TaskStackBuilder.create(mContext);
                    acceptStackBuilder.addParentStack(FrontPage_Activity.class);
                    acceptStackBuilder.addNextIntent(laterIntent);
                    PendingIntent pendingIntent = acceptStackBuilder.getPendingIntent(0, PendingIntent
                            .FLAG_UPDATE_CURRENT);
                    mChatNotificationBuilder.addAction(R.drawable.ic_reply_black_24dp, mContext.getString(R
                            .string.later), pendingIntent);

                }

                activityIntent = new Intent(mContext, FrontPage_Activity.class);
                stackBuilder.addParentStack(FrontPage_Activity.class);
                stackBuilder.addNextIntent(activityIntent);
            }
            PendingIntent chatPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.
                    FLAG_NO_CREATE);
            if (chatPendingIntent == null)

            {
                chatPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_ONE_SHOT |
                        PendingIntent.FLAG_UPDATE_CURRENT);
            }

            mChatNotificationBuilder.setContentIntent(chatPendingIntent);
            mNotificationManager.notify(CHAT_NOTIFICATION_ID, mChatNotificationBuilder.build());
        }
    }


    public void onNewChatReceived(ChatMessage message, String threadId) {
        //
        String remoteParty = message.getRemoteParty();
        ChatThread userThread = getUserFromLocalDb(remoteParty);
        if (userThread != null) {
            ChatMessagesDaoImpl.getInstance().saveChatMessage(mContext, message);
            if (!userThread.getCurrentThreadId().equals(threadId)) {
                userThread.setCurrentThreadId(threadId);
                saveNewThreadId(message.getRemoteParty(), threadId);
            }
        }
        issueChatNotification();
        //getUnreadMessages();
        //ChatMessagesDaoImpl.getInstance().saveChatMessage(mContext, message);
        // mChatNotificationPending = true;
        return;

        //      saveChatTemporarily(message, threadId);
//    mChatNotificationPending=true;
        //    getUserFromRemote(remoteParty);
    }


    public void cancelRosterNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(CHAT_NOTIFICATION_ID);
        }
    }


    public ChatThread getUserFromLocalDb(String remoteParty) {
        ChatThread user = mChatThreads.get(remoteParty);
        if (user == null) {
            ChatThread thread = ChatThreadsDaoImpl.getInstance().getChatThreadDetails(mContext,
                    remoteParty);
            if (thread != null) {

                mChatThreads.put(remoteParty, thread);
                user = thread;
            }
        }

        return user;
    }
}