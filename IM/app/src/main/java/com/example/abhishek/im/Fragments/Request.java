package com.example.abhishek.im.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abhishek.im.ChatUtilityApp;
import com.example.abhishek.im.FrontPage_Activity;
import com.example.abhishek.im.R;
import com.example.abhishek.im.Request_Adapter;
import com.example.abhishek.im.Request_Adapter.ClickListener;
import com.example.smacklibrary.CallBack;
import com.example.smacklibrary.StorageUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by abhishek on 22/6/16.
 */
public class Request extends Fragment {
    //ChatUtilityApp mChatUtility;
    StorageUtil storageUtil;
    RecyclerView recyclerView;
    private Request_Adapter adapter;
    List<String> list;
    ChatUtilityApp chatUtilityApp;
    CallBack callBack;
    CallBack.ExtraCallBacks extraCallBacks = new CallBack.ExtraCallBacks() {
        @Override
        public void newInvite() {
            changeInAdapterAdd();
        }
    };
    Request_Adapter.ClickListener clickListener = new ClickListener() {

        @Override
        public void itemclick(String user, boolean isAccept, int position) {
            ((FrontPage_Activity) getActivity()).InviteStatus(user, isAccept);
            getAllInvites();
            adapter.notifyItemRemoved(position);
        }
    };

    @Nullable
    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.request, container, false);
        storageUtil = new StorageUtil(getActivity());
        list = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        chatUtilityApp = ChatUtilityApp.getInstance(getActivity());
        callBack = CallBack.getInstance(getActivity());
        callBack.register_Extra_Callback(extraCallBacks);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getAllInvites();
        adapter = new Request_Adapter(getActivity(), list, (FrontPage_Activity) getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter.setClickListener(clickListener);
    }

    public void changeInAdapterAdd() {
        getAllInvites();
        adapter.notifyDataSetChanged();
    }

    public void getAllInvites() {
        list.clear();
        Map<String, ?> local_list = storageUtil.getAllInvitation();
        for (Map.Entry<String, ?> entry : local_list.entrySet()) {
            String s = entry.getValue().toString();
            list.add(s);
        }
    }


}
