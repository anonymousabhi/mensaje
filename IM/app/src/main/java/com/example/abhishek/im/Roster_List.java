package com.example.abhishek.im;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.smacklibrary.StorageUtil;
import com.example.smacklibrary.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Roster_List extends AppCompatActivity {
    Toolbar toolbar;
    User user;
    StorageUtil storageUtil;
    ArrayAdapter adapter;
    ListView listview;
    List<String> arraylist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_roster__list);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        storageUtil = new StorageUtil(this);
        user = storageUtil.getUser();
        if (user != null)
            actionBar.setTitle("Welcome " + user.getName());
        arraylist = new ArrayList<>();
        getAllAddedUser();
        listview = (ListView) findViewById(R.id.listview);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arraylist);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Roster_List.this, ChattingActivity.class);
                intent.putExtra("peer", arraylist.get(position));
                intent.putExtra("activity", "list");
                startActivity(intent);
            }
        });
    }

    public void getAllAddedUser() {
        arraylist.clear();
        Map<String, ?> local_list = storageUtil.getAllAddedUSer();
        for (Map.Entry<String, ?> entry : local_list.entrySet()) {
            String s = entry.getValue().toString();
            arraylist.add(s);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, FrontPage_Activity.class));
    }
}
