package com.example.abhishek.im.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.abhishek.im.ChatUtilityApp;
import com.example.abhishek.im.Chat_Adapter;
import com.example.abhishek.im.R;
import com.example.smacklibrary.CallBack;
import com.example.smacklibrary.StorageUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by abhishek on 11/7/16.
 */
public class Chats extends Fragment {
    List<String> list;
    RecyclerView recyclerView;
    Chat_Adapter adapter;
    StorageUtil storageUtil;
    ChatUtilityApp chatUtilityApp;
    CallBack callBack;
    CallBack.AddedUserCallBack addedUserCallBack = new CallBack.AddedUserCallBack() {
        @Override
        public void saveAddedUser() {
            changeInAdapter();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chats, container, false);
        list = new ArrayList<>();
        storageUtil = new StorageUtil(getActivity());
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        chatUtilityApp = ChatUtilityApp.getInstance(getActivity());
        callBack = CallBack.getInstance(getActivity());
        callBack.register_SaveAdded_Callback(addedUserCallBack);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getAllAddedUser();
        adapter = new Chat_Adapter(getActivity(), list);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public void changeInAdapter() {
        getAllAddedUser();
        adapter.notifyDataSetChanged();
    }

    public void getAllAddedUser() {
        list.clear();
        Map<String, ?> local_list = storageUtil.getAllAddedUSer();
        for (Map.Entry<String, ?> entry : local_list.entrySet()) {
            String s = entry.getValue().toString();
            list.add(s);

        }
    }
}
