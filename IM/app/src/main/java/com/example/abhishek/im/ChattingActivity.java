package com.example.abhishek.im;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.smacklibrary.ChatMessage;
import com.example.smacklibrary.ChatMessagesDaoImpl;
import com.example.smacklibrary.LocalDbProvider;
import com.example.smacklibrary.MyAIDL;
import com.example.smacklibrary.SmackService;
import com.example.smacklibrary.StorageUtil;

import java.util.ArrayList;


public class ChattingActivity extends AppCompatActivity {
    public static final String REMOTE_USER = "remote_user";
    private static final String TAG = "Tlt_Smfack";
    Button button_send;
    EditText edittext_send;
    StorageUtil storageUtil;
    ArrayList arrayList;
    ArrayAdapter adapter;
    ListView listview;
    MyObserver observer;
    Toolbar toolbar;
    MyAIDL myAIDL = null;
    ServiceConnection mConnection;
    private ChatUtilityApp mChatUtility;
    private String peer;

    private void init() {

        mConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                myAIDL = MyAIDL.Stub.asInterface(service);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                myAIDL = null;
            }
        };
        if (myAIDL == null) {
            Intent intent = new Intent(ChattingActivity.this, SmackService.class);
            intent.setAction("invite");
            bindService(intent, mConnection, Service.BIND_AUTO_CREATE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbindService(mConnection);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getContentResolver().unregisterContentObserver(observer);

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        mChatUtility = ChatUtilityApp.getInstance(this);
        storageUtil = new StorageUtil(this);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        peer = getIntent().getStringExtra("peer");
        if (peer != null) {
            actionBar.setTitle(peer);
        }
        arrayList = new ArrayList<String>();
        edittext_send = (EditText) findViewById(R.id.edittext_send);
        button_send = (Button) findViewById(R.id.button_send);
        listview = (ListView) findViewById(R.id.listview);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList);
        getAllchats();
        listview.setAdapter(adapter);
        button_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = edittext_send.getText().toString();
                edittext_send.setText("");
                if (!TextUtils.isEmpty(msg)) {
                    try {
                        myAIDL.onNewChatSend(peer, msg);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        observer = new MyObserver(new Handler(), this);
        init();
    }

    public void onNewChatReceived(final ChatMessage message) {
        if (message.getRemoteParty().equals(peer)) {
            Log.i(TAG, "New chat Received");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (message.isTx()) {
                        arrayList.add("Sent Msg- " + message.getMessage());
                    } else {
                        arrayList.add("Receive- " + message.getMessage());
                    }
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getContentResolver().registerContentObserver(LocalDbProvider.URI_CHAT_MESSAGES, true, observer);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.chatting_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                storageUtil.clear();
                startActivity(new Intent(this, MainActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = getIntent();
        String s = intent.getStringExtra("activity");
        if (s.equals("list")) {
            startActivity(new Intent(this, Roster_List.class));
        } else if (s.equals("front")) {
            startActivity(new Intent(this, FrontPage_Activity.class));
        }
    }


    public void getAllchats() {
        ArrayList<ChatMessage> chats = ChatMessagesDaoImpl.getInstance().getAllChats(this, peer);
        if (chats != null) {
            for (ChatMessage cmsg : chats) {
                if (cmsg.isTx()) {
                    arrayList.add("Sent - " + cmsg.getMessage());
                } else {
                    arrayList.add("Received- " + cmsg.getMessage());
                }
            }
        }
        adapter.notifyDataSetChanged();
    }
}
