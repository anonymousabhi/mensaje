package com.example.abhishek.im;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.smacklibrary.ChatMessage;
import com.example.smacklibrary.SmackService;
import com.example.smacklibrary.StorageUtil;
import com.example.smacklibrary.User;

/**
 * Created by abhishek on 4/7/16.
 */
public class ActivityReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent;
        StorageUtil storageUtil;
        switch (intent.getAction()) {
            case Intent.ACTION_BOOT_COMPLETED:
                storageUtil = new StorageUtil(context);
                User user = storageUtil.getUser();
                Log.i("TAG", "Boot");
                if (user != null) {
                    String email = user.getEmail();
                    String password = user.getPassword();
                    String name = user.getName();
                    serviceIntent = new Intent(context, SmackService.class);
                    serviceIntent.putExtra("email", email);
                    serviceIntent.putExtra("password", password);
                    serviceIntent.putExtra("name", name);
                    serviceIntent.putExtra("activity", "receiver");
                    context.startService(serviceIntent);
                }
                break;

            case SmackService.NEW_MESSAGE:
                Log.i("TAG", "processMessage() BroadCast Recieve");
                ChatMessage chat = intent.getParcelableExtra(SmackService.BUNDLE_MESSAGE);
                String threadId = intent.getStringExtra(SmackService.BUNDLE_THREAD_ID);
                ChatUtilityApp.getInstance(context).onNewChatReceived(chat, threadId);
                break;

            default:
                Toast.makeText(context, "default", Toast.LENGTH_SHORT).show();
        }
    }
}
