package com.example.abhishek.im.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.abhishek.im.ChatUtilityApp;
import com.example.abhishek.im.FrontPage_Activity;
import com.example.abhishek.im.R;


/**
 * Created by abhishek on 22/6/16.
 */
public class Invite extends Fragment {
    Button bt_invite;
    EditText et_email;
    TextInputLayout Error_inviteemail;
    private ChatUtilityApp mChatUtilityApp;


    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void generateError(TextInputLayout t, String Message) {
        t.setErrorEnabled(true);
        t.setError(Message);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.invite, container, false);
        bt_invite = (Button) view.findViewById(R.id.bt_sendinvite);
        mChatUtilityApp = ChatUtilityApp.getInstance(getActivity());
        et_email = (EditText) view.findViewById(R.id.et_inviteemail);
        Error_inviteemail = (TextInputLayout) view.findViewById(R.id.til_ivite_et);

        bt_invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String invite_email = et_email.getText().toString();
                if (!isValidEmail(invite_email)) {
                    generateError(Error_inviteemail, "      *Enter a Valid Email");
                } else {
                    et_email.setText("");
                    //invite_email = User.get_id(invite_email);
                    // ChatThread thread = new ChatThread(invite_email, invite_email);
                    //thread.setCurrentThreadId(invite_email);
                    // mChatUtilityApp.onAddUser(thread, true);
                    ((FrontPage_Activity) getActivity()).sendInvite(invite_email, true);
                }

            }
        });
        return view;
    }
}
