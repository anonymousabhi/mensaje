package com.example.abhishek.im;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.widget.Toast;

import com.example.smacklibrary.ChatMessage;
import com.example.smacklibrary.ChatMessagesDaoImpl;
import com.example.smacklibrary.MyApp;

/**
 * Created by abhishek on 6/7/16.
 */
public class MyObserver extends ContentObserver {
    ChattingActivity activity;

    /**
     * Creates a content observer.
     *
     * @param handler The handler to run {@link #onChange} on, or null if none.
     */
    public MyObserver(Handler handler, ChattingActivity activity) {
        super(handler);
        this.activity = activity;
    }

    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange, null);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        ChatMessage message = ChatMessagesDaoImpl.getInstance().getNewMessage(App.getContext());
        activity.onNewChatReceived(message);
    }


}
