// MyAIDL.aidl
package com.example.smacklibrary;

// Declare any non-default types here with import statements

interface MyAIDL {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
    void onAddUser(String user, boolean isAdd) ;
    void isAccepted(String user,boolean isAdd);
    void onNewChatSend(String peer,String msg);
}
