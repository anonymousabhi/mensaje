package com.example.smacklibrary;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by abhishek on 25/7/16.
 */
public class CallBack {
    private static CallBack mInstance;
    CallbackMethods method;
    ExtraCallBacks extraMethod;
    StorageUtil storageUtil;
    private AddedUserCallBack saveAddedUser;

    private CallBack(Context context) {
        storageUtil = new StorageUtil(context);
    }

    public static CallBack getInstance(@NonNull Context context) {
        if (mInstance == null) {
            mInstance = new CallBack(context);
        }

        return mInstance;
    }


    public void register_Callback(CallbackMethods method) {

        this.method = method;
    }

    public void startNewActivity() {
        if (method != null) {
            method.newActivity();
        }
    }

    public void register_Extra_Callback(ExtraCallBacks method) {
        this.extraMethod = method;
    }

    public void sendInvite(String from) {
        if (extraMethod != null) {
            storageUtil.setInvitepreference(User.get_id(from));
            extraMethod.newInvite();
        }
    }

    public void register_SaveAdded_Callback(AddedUserCallBack method) {
        this.saveAddedUser = method;
    }

    public void saveAddedUser(String user_name) {
        if (saveAddedUser != null) {
            storageUtil.setAddUser(User.get_id(user_name));
            saveAddedUser.saveAddedUser();
        }
    }

    public interface AddedUserCallBack {
        void saveAddedUser();
    }

    public interface CallbackMethods {
        void newActivity();
    }

    public interface ExtraCallBacks {
        void newInvite();
    }

}
