package com.example.smacklibrary;

import android.provider.BaseColumns;

public class DatabaseContract {

    public static abstract class ChatThreadDb implements BaseColumns {
        public static final String TABLE_NAME = "chat_thread";
        public static final String COLUMN_NAME_REMOTE_PARTY_NAME = "remote_party_name";
        public static final String COLUMN_NAME_PROFILE_PIC = "profile_pic";
        public static final String COLUMN_NAME_THREAD_ID = "thread_id";
        public static final String COLUMN_NAME_CATEGORY_ID = "category_id";
    }

    public static abstract class ChatMessageDb implements BaseColumns {
        public static final String TABLE_NAME = "chat_message";
        public static final String COLUMN_NAME_REMOTE_PARTY = "remote_party";
        public static final String COLUMN_NAME_MESSAGE = "message";
        public static final String COLUMN_NAME_MESSAGE_ID = "message_id";
        public static final String COLUMN_NAME_MESSAGE_TIME = "message_time";
        public static final String COLUMN_NAME_IS_TX = "is_tx";
        public static final String COLUMN_NAME_IS_UNREAD = "is_unread";
        public static final String COLUMN_NAME_MESSAGE_TYPE = "message_type";
        public static final String COLUMN_NAME_MESSAGE_JSON = "message_json";
        public static final String COLUMN_NAME_SEND_STATE = "send_state";
    }

    public static abstract class NotificationCount implements BaseColumns {
        public static final String TABLE_NAME = "notification_count";
        public static final String COLUMN_NAME_REMOTE_USER_ID = "remote_user_id";
        public static final String COLUMN_NAME_NOTIFICATION_TYPE = "notification_type";
        public static final String COLUMN_NAME_READ_STATUS = "read_status";
    }
}