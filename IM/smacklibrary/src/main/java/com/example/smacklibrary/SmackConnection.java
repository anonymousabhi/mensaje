package com.example.smacklibrary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.StanzaListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.filter.StanzaTypeFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smack.roster.packet.RosterPacket;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.chatstates.ChatState;
import org.jivesoftware.smackx.chatstates.ChatStateListener;
import org.jivesoftware.smackx.chatstates.ChatStateManager;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.forward.packet.Forwarded;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jivesoftware.smackx.json.packet.JsonPacketExtension;
import org.jivesoftware.smackx.offline.OfflineMessageManager;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.privacy.PrivacyList;
import org.jivesoftware.smackx.privacy.PrivacyListManager;
import org.jivesoftware.smackx.privacy.packet.PrivacyItem;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import messagearchive.xep313.MessageArchiveManager;


public class SmackConnection implements ConnectionListener, ChatManagerListener,
        ChatMessageListener, PingFailedListener, RosterListener, StanzaListener, ChatStateListener {

    private static final String NAME_KEY = "Name";
    private static final String EMAIL_KEY = "Email";
    private static final String USER_ID_KEY = "Username";

    private static final String TAG = "Tlt_Smack";
    private static final String BLOCK_LIST_NAME = "blockList";

    private static final int SERVICE_PORT = 9090;
    private static final int REPLY_TIMEOUT = 15000; // 15 seconds
    private static final int PING_INTERVAL = 10 * 60; //10 minutes

    private final Context mApplicationContext;
    private final String mUsername, mPassword, mName, mUserEmail, mServiceName;

    private XMPPTCPConnection mConnection;
    private BroadcastReceiver mReceiver;
    private PrivacyListManager mPrivacyManager;
    private ChatStateManager mChatStateManager;
    private OfflineMessageManager mOfflineManager;
    private PrivacyList mPrivacyList;
    private Roster mRoster;
    Context context;
    StorageUtil storageUtil;

    public SmackConnection(Context pContext, User savedUser, String serverUrl) {
        Log.i(TAG, "SmackConnection()");
        context = pContext;
        mApplicationContext = pContext.getApplicationContext();
        mUsername = savedUser.get_id();
        mPassword = savedUser.getPassword();
        mName = savedUser.getName();
        mUserEmail = savedUser.getEmail();
        mServiceName = serverUrl;
        storageUtil = new StorageUtil(mApplicationContext);
    }

    public void createUser() throws XMPPException, SmackException {
        AccountManager am = AccountManager.getInstance(mConnection);
        Map<String, String> attrs = new HashMap<>();
        attrs.put("username", mUsername);
        attrs.put("password", mPassword);
        attrs.put("name", mName);
        attrs.put("email", mUserEmail);

        am.createAccount(mUsername, mPassword, attrs);
    }
/*
    public List<HashMap<String, String>> searchUsers(String userName) {
        if (mConnection == null || !mConnection.isConnected()) {
            return null;
        }

        HashMap<String, String> user;
        List<HashMap<String, String>> results = new ArrayList<>();
        try {
            UserSearchManager usm = new UserSearchManager(mConnection);
            Form searchForm = usm.getSearchForm("search." + mConnection.getServiceName());
            Form answerForm = searchForm.createAnswerForm();
            UserSearch userSearch = new UserSearch();
            answerForm.setAnswer(USER_ID_KEY, true);
            answerForm.setAnswer("search", userName);
            ReportedData data = userSearch.sendSearchForm(mConnection, answerForm, "search." +
                    mConnection.getServiceName());

            for (ReportedData.Row row : data.getRows()) {
                user = new HashMap<>();
                user.put(USER_ID_KEY, row.getValues(USER_ID_KEY).toString());
                user.put(NAME_KEY, row.getValues(NAME_KEY).toString());
                user.put(EMAIL_KEY, row.getValues(EMAIL_KEY).toString());
                results.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }*/

    private void sendBlockListBroadcast() {
        if (mPrivacyList != null) {
            List<PrivacyItem> usersList = mPrivacyList.getItems();
            ArrayList<String> blockedUsers = new ArrayList<>();
            for (PrivacyItem user : usersList) {
                if (user.getType() == PrivacyItem.Type.jid && !user.isAllow()) {
                    blockedUsers.add(toUserName(user.getValue()));
                }
            }

            if (blockedUsers.size() > 0) {
                Intent intent = new Intent(SmackService.BLOCKED_USERS_LIST);
                intent.setPackage(mApplicationContext.getPackageName());
                intent.putStringArrayListExtra(SmackService.BUNDLE_USER_ID, blockedUsers);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
                }

                mApplicationContext.sendBroadcast(intent);
            }
        }
    }

    private void refreshBlockList() {
        try {
            mPrivacyList = mPrivacyManager.getPrivacyList(BLOCK_LIST_NAME);
            if (mPrivacyList != null) {
                if (!mPrivacyList.isActiveList()) {
                    mPrivacyManager.setActiveListName(BLOCK_LIST_NAME);
                }

                sendBlockListBroadcast();
            }
        } catch (SmackException | XMPPException e) {
            e.printStackTrace();
        }
    }

    public void login(boolean doChatSync) throws IOException, XMPPException, SmackException {
        mConnection.login(mUsername, mPassword);
        mOfflineManager = new OfflineMessageManager(mConnection);

        setupSendMessageReceiver();
        ChatManager.getInstanceFor(mConnection).addChatListener(this);

        refreshBlockList();

        if (doChatSync) {
            checkMessageArchive(null);
            mOfflineManager.deleteMessages();
        } else {
            try {
                getOfflineMessages();
            } catch (SmackException | XMPPException e) {
                e.printStackTrace();
            }
        }
        mConnection.addAsyncStanzaListener(this, StanzaTypeFilter.PRESENCE);

        mConnection.sendStanza(new Presence(Presence.Type.available));
       /* Toast.makeText(mApplicationContext, "Login",
                Toast.LENGTH_LONG).show();*/
    }

/* public void packetlistener() {
        StanzaListener listener = new StanzaListener() {
            @Override
            public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
                Log.i(TAG, "incoming roster request from : " + packet.getFrom());
                Toast.makeText(mApplicationContext, "process", Toast.LENGTH_SHORT).show();

               final Presence presence = (Presence) packet;
                switch (presence.getType()) {
                    case subscribe:
                        incomingRosterRequest(presence.getFrom(), true);
                        break;
                    case unsubscribe:
                        incomingRosterRequest(presence.getFrom(), false);
                        break;
                }
            }
        };
        mConnection.addAsyncStanzaListener(listener, StanzaTypeFilter.PRESENCE);
    }*/


    public void connect() throws IOException, XMPPException, SmackException {
        Log.i(TAG, "connect()");
        //   Toast.makeText(context, "Connect", Toast.LENGTH_LONG).show();
        XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
                .setDebuggerEnabled(true)
                .setServiceName(mServiceName)
                .setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
                .setPort(SERVICE_PORT)
                .setSendPresence(false)
                .build();

        mConnection = new XMPPTCPConnection(config);
        mConnection.setPacketReplyTimeout(REPLY_TIMEOUT);

        //Set ConnectionListener here to catch initial connect();
        mConnection.addConnectionListener(this);
        mConnection.connect();
        //  Toast.makeText(mApplicationContext, "after calling connect", Toast.LENGTH_SHORT).show();
        PingManager.setDefaultPingInterval(PING_INTERVAL);
        PingManager pingManager = PingManager.getInstanceFor(mConnection);
        pingManager.registerPingFailedListener(this);
        //mConnection.addAsyncStanzaListener(this, StanzaTypeFilter.PRESENCE);
        mPrivacyManager = PrivacyListManager.getInstanceFor(mConnection);
        //packetlistener();
        //mRoster.setSubscriptionMode(Roster.SubscriptionMode.accept_all); // reverting back to
        mRoster = Roster.getInstanceFor(mConnection);
        mRoster.setSubscriptionMode(Roster.SubscriptionMode.manual);
        // accept all for now
        mRoster.addRosterListener(this);

        ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(mConnection);
        if (!reconnectionManager.isAutomaticReconnectEnabled()) {
            reconnectionManager.enableAutomaticReconnection();
        }

        mChatStateManager = ChatStateManager.getInstance(mConnection);

        /*DeliveryReceiptManager deliveryReceiptManager = DeliveryReceiptManager.getInstanceFor
                (mConnection);
        if (deliveryReceiptManager.getAutoReceiptMode() != DeliveryReceiptManager.AutoReceiptMode
                .disabled) {
            deliveryReceiptManager.setAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode
                    .disabled);
        }

        deliveryReceiptManager.autoAddDeliveryReceiptRequests();
        deliveryReceiptManager.addReceiptReceivedListener(new ReceiptReceivedListener() {
            public void onReceiptReceived(String fromJid, String toJid, String receiptId, Stanza
                    receipt) {
            }
        });*/
    }

    @Override
    public void stateChanged(Chat chat, ChatState state) {
        SmackService.ChatStateType chatStateType = SmackService.ChatStateType.INACTIVE;
        switch (state) {
            case active:
                chatStateType = SmackService.ChatStateType.ACTIVE;
                break;
            case inactive:
                chatStateType = SmackService.ChatStateType.INACTIVE;
                break;
            case composing:
                chatStateType = SmackService.ChatStateType.TYPING;
                break;
            case paused:
                chatStateType = SmackService.ChatStateType.PAUSED;
                break;
            case gone:
                chatStateType = SmackService.ChatStateType.GONE;
                break;
        }

        Intent intent = new Intent(SmackService.CHAT_STATE_RX);
        intent.setPackage(mApplicationContext.getPackageName());
        intent.putExtra(SmackService.BUNDLE_USER_ID, toUserName(chat.getParticipant()));
        intent.putExtra(SmackService.BUNDLE_CHAT_STATE, chatStateType);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }

        mApplicationContext.sendBroadcast(intent);
    }

    public void disconnect() {
        //  Toast.makeText(context, "disconnect", Toast.LENGTH_LONG).show();
        Log.i(TAG, "disconnect()");
        if (mConnection != null) {
            mConnection.disconnect();
        }
        //Toast.makeText(context, "disconnect one", Toast.LENGTH_LONG).show();
        mConnection = null;
        if (mReceiver != null) {
            mApplicationContext.unregisterReceiver(mReceiver);
            mReceiver = null;
        }
        // Toast.makeText(context, "disconnect two", Toast.LENGTH_LONG).show();
    }

    private void getOfflineMessages() throws SmackException, XMPPException {
        if (mOfflineManager.supportsFlexibleRetrieval()) {
            int count = mOfflineManager.getMessageCount();
            if (count > 0) {
                HashMap<String, String> threadIds = new HashMap<>();
                List<Message> offlineMessageList = mOfflineManager.getMessages();
                mOfflineManager.deleteMessages();

                ArrayList<ChatMessage> offlineMessages = new ArrayList<>(offlineMessageList.size());
                for (Message msg : offlineMessageList) {
                    String from = toUserName(msg.getFrom());

                    try {
                        ChatMessage cMsg = new ChatMessage(from, URLDecoder.decode(msg.getBody(),
                                "utf-8"));
                        cMsg.setMessageId(msg.getStanzaId());
                        cMsg.setIsTx(false);
                        offlineMessages.add(cMsg);

                        if (msg.hasExtension(DelayInformation.ELEMENT, DelayInformation.NAMESPACE)) {
                            DelayInformation delayInformation = msg.getExtension(DelayInformation.
                                    ELEMENT, DelayInformation.NAMESPACE);
                            Date date = delayInformation.getStamp();
                            cMsg.setMessageDateTime(date);
                        } else {
                            cMsg.setMessageDateTime(new Date());
                        }

                        if (msg.hasExtension(JsonPacketExtension.ELEMENT, JsonPacketExtension.NAMESPACE)) {
                            JsonPacketExtension jsonPacketExtension = msg.getExtension
                                    (JsonPacketExtension.ELEMENT, JsonPacketExtension.NAMESPACE);
                            String json = jsonPacketExtension.getJson();
                            ChatMessage.JsonMessage jsonMessage = ChatMessage.getJsonMessage(json);
                            cMsg.setType(ChatMessage.getMessageType(jsonMessage));
                            cMsg.setJsonMessage(jsonMessage);
                        }

                        if (threadIds.containsKey(from) && !threadIds.get(from).equals(msg.getThread())) {
                            threadIds.remove(from);
                        }

                        if (msg.getThread() != null) {
                            threadIds.put(from, msg.getThread());
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }

                Intent intent = new Intent(SmackService.OFFLINE_MESSAGES);
                intent.setPackage(mApplicationContext.getPackageName());
                intent.putParcelableArrayListExtra(SmackService.BUNDLE_MESSAGE, offlineMessages);
                intent.putExtra(SmackService.BUNDLE_THREAD_ID, threadIds);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
                }

                mApplicationContext.sendBroadcast(intent);
            }
        }
    }

    private String toJId(String userName) {
        return TextUtils.isEmpty(userName) ? "" : userName + "@" + mConnection.getServiceName();
    }

    private String toUserName(String JId) {
        return TextUtils.isEmpty(JId) ? "" : JId.substring(0, JId.lastIndexOf("@"));
    }

    private PrivacyItem findUserEntryInBlockList(String userNameToCheck, List<PrivacyItem> items) {
        if (!TextUtils.isEmpty(userNameToCheck) && items != null) {
            for (PrivacyItem item : items) {
                if (item.getType() == PrivacyItem.Type.jid) {
                    String userName = toUserName(item.getValue());
                    if (userName.equals(userNameToCheck)) {
                        return item;
                    }
                }
            }
        }

        return null;
    }

    /*private void blockUser(String toUserId, boolean allowUser) {
        if (mPrivacyList != null) {
            List<PrivacyItem> items = mPrivacyList.getItems();
            PrivacyItem item = findUserEntryInBlockList(toUserId, items);
            if (item != null) {
                if (item.isAllow() == allowUser) {
                    return;
                } else {
                    items.remove(item);
                }
            }

            item = new PrivacyItem(PrivacyItem.Type.jid, toJId(toUserId), allowUser, 11);
            items.add(item);
            try {
                mPrivacyManager.updatePrivacyList(BLOCK_LIST_NAME, items);
                sendBlockListBroadcast();
            } catch (SmackException | XMPPException e) {
                e.printStackTrace();
            }
        } else {
            List<PrivacyItem> privacyItems = new Vector<>();
            PrivacyItem item = new PrivacyItem(PrivacyItem.Type.jid, toJId(toUserId), allowUser,
                    11);
            privacyItems.add(item);

            try {
                mPrivacyManager.createPrivacyList(BLOCK_LIST_NAME, privacyItems);
                mPrivacyManager.setActiveListName(BLOCK_LIST_NAME);
                sendBlockListBroadcast();
            } catch (XMPPException | SmackException e) {
                e.printStackTrace();
            }
        }
    }*/

    private void sendPresenceSubscription(String remoteJid, boolean isSubscribed) {
        Presence response = new Presence(isSubscribed ? Presence.Type.subscribed : Presence.Type
                .unsubscribed);
        response.setTo(remoteJid);
        try {
            mConnection.sendStanza(response);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }


    private void addUser(String remoteUserJid, boolean isAdd) {
        if (mRoster != null) {
            //Toast.makeText(mApplicationContext, "notnull" + "  " + remoteUserJid, Toast.LENGTH_SHORT).show();
            try {
                if (isAdd) {
                    mRoster.createEntry(remoteUserJid, null, null);
                    Toast.makeText(mApplicationContext, "entry created", Toast.LENGTH_SHORT).show();
                } else {
                    RosterEntry entry = mRoster.getEntry(remoteUserJid);
                    if (entry != null) {
                        mRoster.removeEntry(entry);
                    }
                }
            } catch (SmackException.NotLoggedInException | SmackException.NoResponseException |
                    XMPPException.XMPPErrorException | SmackException.NotConnectedException e) {
                Toast.makeText(mApplicationContext, "exception", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else {
        }
    }


    private void setupSendMessageReceiver() throws SmackException.NotConnectedException {
        mReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                //Toast.makeText(MyApp.getContext(), "In setup", Toast.LENGTH_SHORT).show();
                String action = intent.getAction();
                switch (action) {
                    case SmackService.SEND_MESSAGE:
                        ChatMessage message = intent.getParcelableExtra(SmackService
                                .BUNDLE_MESSAGE);
                        String threadId = intent.getStringExtra(SmackService.BUNDLE_THREAD_ID);
                        // Toast.makeText(MyApp.getContext(), "In SEND MESSAGE", Toast.LENGTH_SHORT).show();
                        sendMessage(message, threadId);
                        break;
                    /*case SmackService.BLOCK_USER:
                        String blockedUser = intent.getStringExtra(SmackService.BUNDLE_USER_ID);
                        boolean isBlock = intent.getBooleanExtra(SmackService.BUNDLE_IS_BLOCK,
                                true);
                        blockUser(blockedUser, !isBlock);
                        break;*/
                    case SmackService.NEW_ROSTER:
                        //  Toast.makeText(MyApp.getContext(), "New Roster", Toast.LENGTH_SHORT).show();
                        String addedUser = intent.getStringExtra(SmackService.BUNDLE_USER_ID);
                        boolean isAdd = intent.getBooleanExtra(SmackService.BUNDLE_IS_ADD, true);
                        addUser(toJId(addedUser), isAdd);

                        break;
                    case SmackService.ROSTER_RESPONSE:
                        /*String remoteJid = toJId(intent.getStringExtra(SmackService
                                .BUNDLE_USER_ID));*/
                        String remoteJid = intent.getStringExtra(SmackService
                                .BUNDLE_USER_ID);
                        boolean isAccept = intent.getBooleanExtra(SmackService.BUNDLE_IS_ACCEPT,
                                true);
                        incomingRosterRequest(toJId(remoteJid), isAccept);
                        break;
                    case SmackService.CHAT_STATE_TX:
                        String chatThreadId = intent.getStringExtra(SmackService.BUNDLE_THREAD_ID);
                        SmackService.ChatStateType chatState = (SmackService.ChatStateType)
                                intent.getSerializableExtra(SmackService.BUNDLE_CHAT_STATE);
                        handleChatStateChange(chatThreadId, chatState);
                        break;
                    case SmackService.SEND_FAILED_MESSAGES:
                        Date closeTime = (Date) intent.getSerializableExtra(SmackService
                                .BUNDLE_CLOSE_TIME);
                        if (intent.hasExtra(SmackService.BUNDLE_MESSAGE)) {
                            @SuppressWarnings("unchecked")
                            HashMap<String, ArrayList<ChatMessage>> messages = (HashMap<String,
                                    ArrayList<ChatMessage>>) intent.getSerializableExtra
                                    (SmackService.BUNDLE_MESSAGE);
                            sendFailedMessages(closeTime, messages);
                        } else {
                            sendFailedMessages(closeTime, null);
                        }
                        break;
                }
            }
        };

        IntentFilter filter = new IntentFilter(SmackService.SEND_MESSAGE);
        filter.addAction(SmackService.BLOCK_USER);
        filter.addAction(SmackService.NEW_ROSTER);
        filter.addAction(SmackService.ROSTER_RESPONSE);
        filter.addAction(SmackService.CHAT_STATE_TX);
        filter.addAction(SmackService.SEND_FAILED_MESSAGES);
        mApplicationContext.registerReceiver(mReceiver, filter);
    }

    private void sendFailedMessages(Date closeTime, HashMap<String, ArrayList<ChatMessage>>
            failedMessagesMap) {
        if (closeTime != null) {
            checkMessageArchive(closeTime);
        }

        if (failedMessagesMap != null) {
            Set<String> keySet = failedMessagesMap.keySet();
            for (String key : keySet) {
                ArrayList<ChatMessage> messages = failedMessagesMap.get(key);
                for (ChatMessage message : messages) {
                    sendMessage(message, key);
                }
            }
        }
    }

    private void handleChatStateChange(String threadId, SmackService.ChatStateType chatState) {
        Chat chat = ChatManager.getInstanceFor(mConnection).getThreadChat(threadId);
        if (chat != null) {
            ChatState state;
            switch (chatState) {
                case TYPING:
                    state = ChatState.composing;
                    break;
                case PAUSED:
                    state = ChatState.paused;
                    break;
                case ACTIVE:
                    state = ChatState.active;
                    break;
                case GONE:
                    state = ChatState.gone;
                    break;
                case INACTIVE:
                    state = ChatState.inactive;
                    break;
                default:
                    return;
            }

            try {
                mChatStateManager.setCurrentState(state, chat);
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendMessage(ChatMessage chatMessage, String threadId) {

        Log.i(TAG, "sendMessage() : threadId : " + threadId);
        //Toast.makeText(MyApp.getContext(), "In SendMeSSage", Toast.LENGTH_SHORT).show();
        boolean isError = false;
        if (!TextUtils.isEmpty(threadId)) {
            Chat chat = ChatManager.getInstanceFor(mConnection).getThreadChat(threadId);
            if (chat == null) {
                chat = ChatManager.getInstanceFor(mConnection).createChat(toJId(chatMessage
                        .getRemoteParty()), threadId, this);
            }

            try {
                Message message = new Message();
                message.setStanzaId(chatMessage.getMessageId());
                String msg = URLEncoder.encode(chatMessage.getMessage(), "utf-8");
                msg = msg.replaceAll("\\+", "%20");
                message.setBody(msg);
                if (chatMessage.getJsonMessage() != null) {
                    JsonPacketExtension jsonPacketExtension = new JsonPacketExtension(ChatMessage
                            .createJsonMessage(chatMessage.getJsonMessage()));
                    message.addExtension(jsonPacketExtension);
                }

                chat.sendMessage(message);
                Toast.makeText(mApplicationContext, "Message send", Toast.LENGTH_SHORT).show();
            } catch (SmackException.NotConnectedException | UnsupportedEncodingException e) {
                e.printStackTrace();
                isError = true;
            }

            chatMessage.setSendState(isError ? ChatMessage.SendState.FAILED : ChatMessage
                    .SendState.SENT);

            Intent intent = new Intent(SmackService.MESSAGE_SEND_STATUS);
            intent.setPackage(mApplicationContext.getPackageName());
            intent.putExtra(SmackService.BUNDLE_MESSAGE, chatMessage);
            intent.putExtra(SmackService.BUNDLE_THREAD_ID, threadId);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
            }

            mApplicationContext.sendBroadcast(intent);
        }
    }

    @Override
    public void chatCreated(Chat chat, boolean createdLocally) {
        Log.i(TAG, "chatCreated()");
        chat.addMessageListener(this);
    }

    private void handleIncomingMessage(Message message) {
//        Toast.makeText(mApplicationContext, "In handle", Toast.LENGTH_SHORT).show();
        if (message.getType().equals(Message.Type.chat) || message.getType().equals(Message.Type.
                normal)) {
            if (message.getBody() != null) {
                Intent intent = new Intent(SmackService.NEW_MESSAGE);
                intent.setPackage(mApplicationContext.getPackageName());
                String from = toUserName(message.getFrom());

                try {
                    ChatMessage msg = new ChatMessage(from, URLDecoder.decode(message.getBody(),
                            "utf-8"));
                    msg.setMessageId(message.getStanzaId());
                    msg.setIsTx(false);
                    msg.setMessageDateTime(new Date());
                    intent.putExtra(SmackService.BUNDLE_MESSAGE, msg);
                    intent.putExtra(SmackService.BUNDLE_THREAD_ID, message.getThread());
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
                    }

                    if (message.hasExtension(JsonPacketExtension.ELEMENT, JsonPacketExtension.NAMESPACE)) {
                        JsonPacketExtension jsonPacketExtension = message.getExtension
                                (JsonPacketExtension.ELEMENT, JsonPacketExtension.NAMESPACE);
                        String json = jsonPacketExtension.getJson();
                        ChatMessage.JsonMessage jsonMessage = ChatMessage.getJsonMessage(json);
                        msg.setType(ChatMessage.getMessageType(jsonMessage));
                        msg.setJsonMessage(jsonMessage);
                    }

                    mApplicationContext.sendOrderedBroadcast(intent, null);

                    //mApplicationContext.sendBroadcast(intent, null);
                    Log.i(TAG, "processMessage() BroadCast send");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                /*if (DeliveryReceiptManager.hasDeliveryReceiptRequest(message)) {
                    Message receiptMessage = DeliveryReceiptManager.receiptMessageFor(message);
                    if (receiptMessage != null && TextUtils.isEmpty(receiptMessage.getBody())) {
                        //receiptMessage.setBody("-");
                        Chat chat = ChatManager.getInstanceFor(mConnection).getThreadChat(message
                                .getThread());
                        try {
                            chat.sendMessage(receiptMessage);
                        } catch (SmackException.NotConnectedException e) {
                            e.printStackTrace();
                        }
                    }
                }*/
            }
        }
    }

    private void checkMessageArchive(Date startTime) {
        MessageArchiveManager mam = MessageArchiveManager.getInstanceFor(mConnection);
        List<Forwarded> archivedMessages = mam.getArchivedMessages(0, startTime, null, null);
        if (archivedMessages != null) {
            HashMap<String, String> threadIds = new HashMap<>();
            ArrayList<ChatMessage> messages = new ArrayList<>(archivedMessages.size());

            for (Forwarded archivedMessage : archivedMessages) {
                Message msg = (Message) archivedMessage.getForwardedPacket();
                String from = toUserName(msg.getFrom());
                boolean isTx = from.equals(mUsername);
                String remoteParty = isTx ? toUserName(msg.getTo()) : from;

                try {
                    ChatMessage cMsg = new ChatMessage(remoteParty, URLDecoder.decode(msg.getBody(),
                            "utf-8"));
                    cMsg.setMessageId(msg.getStanzaId());
                    cMsg.setIsUnread(false);
                    cMsg.setSendState(ChatMessage.SendState.SENT);
                    cMsg.setIsTx(isTx);
                    messages.add(cMsg);

                    DelayInformation delayInformation = archivedMessage.getDelayInformation();
                    Date date = delayInformation.getStamp();
                    cMsg.setMessageDateTime(date);

                    if (msg.hasExtension(JsonPacketExtension.ELEMENT, JsonPacketExtension.NAMESPACE)) {
                        JsonPacketExtension jsonPacketExtension = msg.getExtension
                                (JsonPacketExtension.ELEMENT, JsonPacketExtension.NAMESPACE);
                        String json = jsonPacketExtension.getJson();
                        ChatMessage.JsonMessage jsonMessage = ChatMessage.getJsonMessage(json);
                        cMsg.setType(ChatMessage.getMessageType(jsonMessage));
                        cMsg.setJsonMessage(jsonMessage);
                    }

                    if (threadIds.containsKey(remoteParty) && !threadIds.get(remoteParty).equals(msg.
                            getThread())) {
                        threadIds.remove(remoteParty);
                    }

                    if (msg.getThread() != null) {
                        threadIds.put(remoteParty, msg.getThread());
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            Intent intent = new Intent(SmackService.MESSAGE_SYNC);
            intent.setPackage(mApplicationContext.getPackageName());
            intent.putParcelableArrayListExtra(SmackService.BUNDLE_MESSAGE, messages);
            intent.putExtra(SmackService.BUNDLE_THREAD_ID, threadIds);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
            }

            mApplicationContext.sendBroadcast(intent);
        }
    }

    @Override
    public void processMessage(Chat chat, Message message) {
        Log.i(TAG, "processMessage()" + message.getBody());
        handleIncomingMessage(message);
    }


    @Override
    public void connected(XMPPConnection connection) {
        SmackService.sConnectionState = SmackService.ConnectionState.CONNECTED;
        Log.i(TAG, "connected()");
    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        SmackService.sConnectionState = SmackService.ConnectionState.CONNECTED;
        Log.i(TAG, "authenticated()");
    }

    @Override
    public void connectionClosed() {
        SmackService.sConnectionState = SmackService.ConnectionState.DISCONNECTED;
        Log.i(TAG, "connectionClosed()");
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        SmackService.sConnectionState = SmackService.ConnectionState.CLOSED_ON_ERROR;
        Intent intent = new Intent(SmackService.CONNECTION_STATE_CHANGED);
        intent.setPackage(mApplicationContext.getPackageName());
        intent.putExtra(SmackService.BUNDLE_CONNECTION_STATE, SmackService.ConnectionState
                .CLOSED_ON_ERROR);
        intent.putExtra(SmackService.BUNDLE_CLOSE_TIME, System.currentTimeMillis());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }

        mApplicationContext.sendBroadcast(intent);
        Log.i(TAG, "connectionClosedOnError()");
    }

    @Override
    public void reconnectingIn(int seconds) {
        SmackService.sConnectionState = SmackService.ConnectionState.RECONNECTING;
        Log.i(TAG, "reconnectingIn() : " + seconds + " seconds");
    }

    @Override
    public void reconnectionSuccessful() {
        SmackService.sConnectionState = SmackService.ConnectionState.RECONNECTED;
        try {
            mConnection.sendStanza(new Presence(Presence.Type.available));
            mOfflineManager.deleteMessages();
        } catch (SmackException.NotConnectedException | XMPPException.XMPPErrorException |
                SmackException.NoResponseException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(SmackService.CONNECTION_STATE_CHANGED);
        intent.setPackage(mApplicationContext.getPackageName());
        intent.putExtra(SmackService.BUNDLE_CONNECTION_STATE, SmackService.ConnectionState
                .RECONNECTED);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }

        mApplicationContext.sendBroadcast(intent);
        Log.i(TAG, "reconnectionSuccessful()");
    }

    @Override
    public void reconnectionFailed(Exception e) {
        SmackService.sConnectionState = SmackService.ConnectionState.DISCONNECTED;
        Log.i(TAG, "reconnectionFailed()");
    }

    @Override
    public void pingFailed() {
        Log.i(TAG, "pingFailed()");
    }

    private void updateBuddyList(Collection<RosterEntry> entries) {
        ArrayList<ChatBuddy> buddies = new ArrayList<>(entries.size());
        for (RosterEntry entry : entries) {
            RosterPacket.ItemStatus status = entry.getStatus();
            RosterPacket.ItemType type = entry.getType();

            ChatBuddy buddy = new ChatBuddy(toUserName(entry.getUser()));
            /*if (status == RosterPacket.ItemStatus.SUBSCRIPTION_PENDING) {
                buddy.setConversationStatus(ChatBuddy.ConversationStatus.INVITE_SENT);
            } else if (type == RosterPacket.ItemType.to) {
                buddy.setConversationStatus(ChatBuddy.ConversationStatus.INVITE_RECEIVED);
            } else if (type == RosterPacket.ItemType.both) {
                buddy.setConversationStatus(ChatBuddy.ConversationStatus.IN_CONVERSATION);
            }*/

            Presence presence = mRoster.getPresence(entry.getUser());
            if (presence.getType() == Presence.Type.available && (presence.getMode() == Presence
                    .Mode.available || presence.getMode() == Presence.Mode.chat)) {
                buddy.setPresenceStatus(ChatBuddy.PresenceStatus.ONLINE);
            }

            buddies.add(buddy);
        }

        Intent intent = new Intent(SmackService.ROSTER_LIST);
        intent.setPackage(mApplicationContext.getPackageName());
        intent.putParcelableArrayListExtra(SmackService.BUNDLE_ROSTER, buddies);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }

        mApplicationContext.sendBroadcast(intent);
    }

    private void incomingRosterRequest(String userJid, boolean isAdd) {
        Log.i(TAG, "incoming" + userJid);

        //Toast.makeText(mApplicationContext, "incoing", Toast.LENGTH_LONG).show();
        if (isAdd) {
            if (mRoster.contains(userJid)) {
                RosterEntry entry = mRoster.getEntry(userJid);
                RosterPacket.ItemType type = entry.getType();
                RosterPacket.ItemStatus status = entry.getStatus();
                if (status == RosterPacket.ItemStatus.SUBSCRIPTION_PENDING) {
                    sendPresenceSubscription(userJid, true);
                }
            } else {
                addUser(userJid, true);
                sendPresenceSubscription(userJid, true);
                //sendPresenceSubscription(userJid, true);
                /*Intent intent = new Intent(SmackService.INCOMING_ROSTER);
                intent.setPackage(mApplicationContext.getPackageName());
                intent.putExtra(SmackService.BUNDLE_USER_ID, toUserName(userJid));
                mApplicationContext.sendBroadcast(intent);*/
            }
        } else {
            addUser(userJid, false);
        }
        //addUser(userJid, isAdd);
    }

    @Override
    public void entriesAdded(Collection<String> addresses) {
        Log.i(TAG, "SmackConnection() : entriesAdded");
        updateBuddyList(mRoster.getEntries());
    }

    @Override
    public void entriesUpdated(Collection<String> addresses) {
        Log.i(TAG, "SmackConnection() : entriesUpdated");
        updateBuddyList(mRoster.getEntries());
    }

    @Override
    public void entriesDeleted(Collection<String> addresses) {
        Log.i(TAG, "SmackConnection() : entriesDeleted");
        updateBuddyList(mRoster.getEntries());
    }

    @Override
    public void presenceChanged(Presence presence) {
        Log.i(TAG, "SmackConnection() : presenceChanged : from : " + presence.getFrom() +
                ", to : " + presence.getTo());
        updateBuddyList(mRoster.getEntries());
    }

    @Override
    public void processPacket(Stanza packet) throws SmackException.NotConnectedException {
        final Presence presence = (Presence) packet;
        Log.i(TAG, "incoming roster request from : " + presence.getType() + presence.getFrom());
        Intent intent = null;
        switch (presence.getType()) {
            case subscribe:
                if (mRoster.contains(presence.getFrom())) {
                    sendPresenceSubscription(presence.getFrom(), true);
                } else {
                    intent = new Intent(SmackService.INVITE_PACKET);
                    intent.setPackage(mApplicationContext.getPackageName());
                    intent.putExtra(SmackService.PRESENCE_FROM, presence.getFrom());
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
                    }
                    mApplicationContext.sendBroadcast(intent, null);
                    // CallBack.getInstance(mApplicationContext).sendInvite(presence.getFrom());
                }
                break;
            case subscribed:
                intent = new Intent(SmackService.INVITE_ACCEPTED);
                intent.putExtra("user", presence.getFrom());
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
                }
                //sendPresenceSubscription(presence.getFrom(), true);
                mApplicationContext.sendBroadcast(intent, null);
                break;
            case unsubscribed:
                intent = new Intent(SmackService.INVITE_REJECTED);
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
                }
                mApplicationContext.sendBroadcast(intent, null);
                break;
            case unsubscribe:
                //incomingRosterRequest(presence.getFrom(), false);
                break;
        }
    }

}
