package com.example.smacklibrary;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;


public class LocalDbProvider extends ContentProvider {
    private static final int CHAT_THREAD_ID = 100;
    private static final int CHAT_MESSAGE_ID = 101;
    private static final int NOTIFICATION_ID = 103;
    private static final int CHAT_MESSAGE_SPECIFIC_ID = 102;

    private static final String AUTHORITY = BuildConfig.APPLICATION_ID;//+ ".storage";
    private static final String LAST_CHAT_MESSAGE = "last.message";

    public static final Uri URI_CHAT_THREADS = Uri.parse("content://" + AUTHORITY + "/" +
            DatabaseContract.ChatThreadDb.TABLE_NAME);
    public static final Uri URI_CHAT_MESSAGES = Uri.parse("content://" + AUTHORITY + "/" +
            DatabaseContract.ChatMessageDb.TABLE_NAME);
    public static final Uri URI_NOTIFICATION = Uri.parse("content://" + AUTHORITY + "/" +
            DatabaseContract.NotificationCount.TABLE_NAME);
    public static final Uri URI_LAST_CHAT_MESSAGE = Uri.parse("content://" + AUTHORITY + "/" +
            DatabaseContract.ChatMessageDb.TABLE_NAME + "/" + LAST_CHAT_MESSAGE);

    Context context = getContext();
    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, DatabaseContract.ChatThreadDb.TABLE_NAME, CHAT_THREAD_ID);
        sURIMatcher.addURI(AUTHORITY, DatabaseContract.ChatMessageDb.TABLE_NAME, CHAT_MESSAGE_ID);
        sURIMatcher.addURI(AUTHORITY, DatabaseContract.NotificationCount.TABLE_NAME, NOTIFICATION_ID);
        sURIMatcher.addURI(AUTHORITY, DatabaseContract.ChatMessageDb.TABLE_NAME + "/*",
                CHAT_MESSAGE_SPECIFIC_ID);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    @SuppressWarnings("ConstantConditions")
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        int uriType = sURIMatcher.match(uri);
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        SQLiteDatabase sqlDb = DatabaseHelper.getDatabase(false, context);
        String table, limit = null;

        switch (uriType) {
            case CHAT_THREAD_ID:
                table = DatabaseContract.ChatThreadDb.TABLE_NAME;
                break;
            case CHAT_MESSAGE_ID:
                table = DatabaseContract.ChatMessageDb.TABLE_NAME;
                break;
            case CHAT_MESSAGE_SPECIFIC_ID:
                if (uri.getLastPathSegment().equals(LAST_CHAT_MESSAGE)) {
                    table = DatabaseContract.ChatMessageDb.TABLE_NAME;
                    limit = "1";
                } else {
                    throw new IllegalArgumentException("Unknown URI : " + uri);
                }
                break;
            case NOTIFICATION_ID:
                table = DatabaseContract.NotificationCount.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI : " + uri);
        }

        queryBuilder.setTables(table);
        Cursor cursor = queryBuilder.query(sqlDb, projection, selection, selectionArgs, null,
                null, sortOrder, limit);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    @SuppressWarnings("ConstantConditions")
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        long id;
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDb = DatabaseHelper.getDatabase(true, context);
        String table;

        switch (uriType) {
            case CHAT_THREAD_ID:
                table = DatabaseContract.ChatThreadDb.TABLE_NAME;
                break;
            case CHAT_MESSAGE_ID:
                table = DatabaseContract.ChatMessageDb.TABLE_NAME;
                break;
            case NOTIFICATION_ID:
                table = DatabaseContract.NotificationCount.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI : " + uri);
        }
        id = sqlDb.insertWithOnConflict(table, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(uri.getLastPathSegment() + "/" + id);
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        int count = values.length, uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDb = DatabaseHelper.getDatabase(true, context);
        String table;

        switch (uriType) {
            case CHAT_THREAD_ID:
                table = DatabaseContract.ChatThreadDb.TABLE_NAME;
                break;
            case CHAT_MESSAGE_ID:
                table = DatabaseContract.ChatMessageDb.TABLE_NAME;
                break;
            case NOTIFICATION_ID:
                table = DatabaseContract.NotificationCount.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI : " + uri);
        }

        for (ContentValues value : values) {
            sqlDb.insert(table, null, value);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        int rowsDeleted, uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDb = DatabaseHelper.getDatabase(true, context);
        String table;

        switch (uriType) {
            case CHAT_THREAD_ID:
                table = DatabaseContract.ChatThreadDb.TABLE_NAME;
                break;
            case CHAT_MESSAGE_ID:
                table = DatabaseContract.ChatMessageDb.TABLE_NAME;
                break;
            case NOTIFICATION_ID:
                table = DatabaseContract.NotificationCount.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI : " + uri);
        }

        rowsDeleted = sqlDb.delete(table, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase sqlDB = DatabaseHelper.getDatabase(true, context);
        int rowsUpdated, uriType = sURIMatcher.match(uri);
        String table;

        switch (uriType) {
            case CHAT_THREAD_ID:
                table = DatabaseContract.ChatThreadDb.TABLE_NAME;
                break;
            case CHAT_MESSAGE_ID:
                table = DatabaseContract.ChatMessageDb.TABLE_NAME;
                break;
            case NOTIFICATION_ID:
                table = DatabaseContract.NotificationCount.TABLE_NAME;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI : " + uri);
        }

        rowsUpdated = sqlDB.update(table, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }
}
