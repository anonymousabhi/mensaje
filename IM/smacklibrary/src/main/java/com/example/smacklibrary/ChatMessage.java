package com.example.smacklibrary;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

import java.io.Serializable;
import java.util.Date;

public class ChatMessage implements Parcelable {

    public enum SendState {
        SENDING,
        FAILED,
        SENT
    }

    public enum Type {
        INVITE,
        INVITE_ACCEPTED,
        CONVERSATION_CLOSED,
        INVITE_REJECTED,
        TEXT,
        CART_OFFER,
        CART_UPDATE,
        ORDER,
        ORDER_CANCEL,
        OFFER_EXPIRED,
        ORDER_COMPLETED,
        DATE;

        public static String getTypeValue(Type type) {
            return String.valueOf(type.ordinal() + 1);
        }
    }

    private String message;
    private Date messageDateTime;
    private String remoteParty;
    private boolean isTx = true;
    private boolean isUnread = true;
    private Type type = Type.TEXT;
    private JsonMessage jsonMessage;
    private String messageId;
    private SendState sendState = SendState.SENDING;

    public ChatMessage(String remoteParty, String message) {
        this.remoteParty = remoteParty;
        this.message = message == null ? "-" : message;
    }

    public String getRemoteParty() {
        return remoteParty;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isTx() {
        return isTx;
    }

    public void setIsTx(boolean isTx) {
        this.isTx = isTx;
    }

    public boolean isUnread() {
        return isUnread;
    }

    public void setIsUnread(boolean isUnread) {
        this.isUnread = isUnread;
    }

    public Date getMessageDateTime() {
        return messageDateTime;
    }

    public void setMessageDateTime(Date messageDateTime) {
        this.messageDateTime = messageDateTime;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public JsonMessage getJsonMessage() {
        return jsonMessage;
    }

    public void setJsonMessage(JsonMessage jsonMessage) {
        this.jsonMessage = jsonMessage;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public SendState getSendState() {
        return sendState;
    }

    public void setSendState(SendState sendState) {
        this.sendState = sendState;
    }

    public String getTypeThreadMessage(Context context) {
        String typeMessage = "";
        switch (type) {
            case INVITE:
                typeMessage = isTx ? context.getString(R.string.thread_invite_sent) : context
                        .getString(R.string.thread_invite_received);
                break;
            case INVITE_ACCEPTED:
                typeMessage = context.getString(R.string.thread_invite_accepted);
                break;
            case INVITE_REJECTED:
                typeMessage = context.getString(R.string.thread_invite_declined);
                break;
            case CONVERSATION_CLOSED:
                typeMessage = context.getString(R.string.thread_chat_closed);
                break;
            case CART_OFFER:
                typeMessage = isTx ? context.getString(R.string.thread_offer_sent) : context
                        .getString(R.string.thread_offer_received);
                break;
            case CART_UPDATE:
                typeMessage = context.getString(R.string.thread_offer_update);
                break;
            case ORDER:
                typeMessage = context.getString(R.string.thread_order_placed);
                break;
            case ORDER_CANCEL:
                typeMessage = context.getString(R.string.thread_order_canceled);
                break;
            case OFFER_EXPIRED:
                typeMessage = context.getString(R.string.thread_offer_expired);
                break;
            case ORDER_COMPLETED:
                typeMessage = context.getString(R.string.thread_order_completed);
                break;
        }

        return typeMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeString(messageId);
        dest.writeSerializable(messageDateTime);
        dest.writeString(remoteParty);
        dest.writeString(type.toString());
        dest.writeString(sendState.toString());
        boolean[] arr = new boolean[2];
        arr[0] = isTx;
        arr[1] = isUnread;
        dest.writeBooleanArray(arr);
        dest.writeSerializable(jsonMessage);
    }

    public static final Creator CREATOR = new Creator() {
        public ChatMessage createFromParcel(Parcel in) {
            return new ChatMessage(in);
        }

        public ChatMessage[] newArray(int size) {
            return new ChatMessage[size];
        }
    };

    private ChatMessage(Parcel in) {
        message = in.readString();
        messageId = in.readString();
        messageDateTime = (Date) in.readSerializable();
        remoteParty = in.readString();
        type = Type.valueOf(in.readString());
        sendState = SendState.valueOf(in.readString());
        boolean[] arr = new boolean[2];
        in.readBooleanArray(arr);
        isTx = arr[0];
        isUnread = arr[1];
        jsonMessage = (JsonMessage) in.readSerializable();
    }

    public static String createJsonMessage(JsonMessage jsonMessage) {
        Gson gson = new Gson();
        return gson.toJson(jsonMessage);
    }

    public static Type getMessageType(JsonMessage jsonMessage) {
        try {
            int type = Integer.parseInt(jsonMessage.getType());
            Type[] values = Type.values();
            if (type >= 0 && type < values.length) {
                return values[type - 1]; // -1, because the stupid values start from 1
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        return Type.TEXT;
    }

    public static JsonMessage getJsonMessage(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, JsonMessage.class);
    }

    public static class JsonMessage implements Serializable {
        private String type;
        private String orderId;
        private String expiredByMerchant;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getExpiredByMerchant() {
            return expiredByMerchant;
        }

        public void setExpiredByMerchant(String expiredByMerchant) {
            this.expiredByMerchant = expiredByMerchant;
        }
    }
}
