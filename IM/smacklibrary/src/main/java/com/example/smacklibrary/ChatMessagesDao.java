package com.example.smacklibrary;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Date;

public abstract class ChatMessagesDao extends BaseDao {
    public abstract void saveChatMessage(@NonNull Context context,
                                         @NonNull final ChatMessage message);

    public abstract void saveChatMessages(@NonNull Context context,
                                          @NonNull final ArrayList<ChatMessage> messages);

    public abstract ArrayList<ChatMessage> getAllChats(@NonNull Context context,
                                                       @NonNull final String remoteParty);

    public abstract ArrayList<ChatMessage> getAllChats(@NonNull Context context,
                                                       @NonNull final String remoteParty, final
                                                       Date time);

    public abstract ChatMessage getLastReceivedChat(@NonNull Context context,
                                                    @NonNull final String remoteParty);

    public abstract ArrayList<ChatMessage> getAllUnreadChats(@NonNull Context context);

    public abstract int getAllUnreadChatsCount(@NonNull Context context,
                                               @NonNull String from);

    public abstract int markThreadAsRead(@NonNull Context context,
                                         @NonNull final String remoteParty);

    public abstract void deleteAllChats(@NonNull Context context);

    public abstract void changeMessageSendState(@NonNull Context context,
                                                @NonNull final ChatMessage message);

    public abstract ArrayList<ChatMessage> getAllFailedChats(@NonNull Context context);
}
