package com.example.smacklibrary;

import android.os.Parcel;
import android.os.Parcelable;

public class ChatBuddy implements Parcelable {

    public static final Creator CREATOR = new Creator() {
        public ChatBuddy createFromParcel(Parcel in) {
            return new ChatBuddy(in);
        }

        public ChatBuddy[] newArray(int size) {
            return new ChatBuddy[size];
        }
    };
    private String userId;
    private ConversationStatus conversationStatus = ConversationStatus.NONE;
    private PresenceStatus presenceStatus = PresenceStatus.OFFLINE;

    public ChatBuddy(String userId) {
        this.userId = userId;
    }

    private ChatBuddy(Parcel in) {
        userId = in.readString();
        conversationStatus = ConversationStatus.valueOf(in.readString());
        presenceStatus = PresenceStatus.valueOf(in.readString());
    }

    public String getUserId() {
        return userId;
    }

    public ConversationStatus getConversationStatus() {
        return conversationStatus;
    }

    public void setConversationStatus(ConversationStatus conversationStatus) {
        this.conversationStatus = conversationStatus;
    }

    public PresenceStatus getPresenceStatus() {
        return presenceStatus;
    }

    public void setPresenceStatus(PresenceStatus presenceStatus) {
        this.presenceStatus = presenceStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(conversationStatus.toString());
        dest.writeString(presenceStatus.toString());
    }

    public enum ConversationStatus {
        NONE,
        INVITE_SENT,
        INVITE_RECEIVED,
        IN_CONVERSATION,
        ORDER,
        OFFER_EXPIRED,
        CHAT_CLOSED
    }

    public enum PresenceStatus {
        OFFLINE,
        ONLINE
    }
}
