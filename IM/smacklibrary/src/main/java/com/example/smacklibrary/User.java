package com.example.smacklibrary;

/**
 * Created by abhishek on 3/6/16.
 */
public class User {
    String _id, name, email, password;

    public User(String email, String password, String name) {
        this._id = get_id(email);
        this.email = email;
        this.password = password;
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String get_id() {
        String tempemail = "";
        for (int i = 0; i < email.length(); i++) {
            if (email.charAt(i) == '@') {
                break;
            } else {
                tempemail = tempemail + email.charAt(i);
            }
        }
        return tempemail;
    }

    public static String get_id(String email) {
        String tempemail = "";
        if (email != null) {
            for (int i = 0; i < email.length(); i++) {
                if (email.charAt(i) == '@') {
                    break;
                } else {
                    tempemail = tempemail + email.charAt(i);
                }
            }
        }
        return tempemail;
    }
}
