package com.example.smacklibrary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.example.smacklibrary.DatabaseContract.ChatThreadDb;

import java.util.ArrayList;


public class ChatThreadsDaoImpl extends ChatThreadsDao {
    private static ChatThreadsDaoImpl mInstance;

    private ChatThreadsDaoImpl() {

    }

    public static ChatThreadsDaoImpl getInstance() {
        if (mInstance == null) {
            mInstance = new ChatThreadsDaoImpl();
        }

        return mInstance;
    }

    public String getCreateTableSQL() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append(ChatThreadDb.TABLE_NAME)
                .append(" (")
                .append(ChatThreadDb._ID)
                .append(TEXT_TYPE)
                .append(PRIMARY_KEY)
                .append(COMMA_SEP)
                .append(ChatThreadDb.COLUMN_NAME_THREAD_ID)
                .append(TEXT_TYPE)
                .append(NOT_NULL)
                .append(COMMA_SEP)
                .append(ChatThreadDb.COLUMN_NAME_CATEGORY_ID)
                .append(TEXT_TYPE)
                .append(COMMA_SEP)
                .append(ChatThreadDb.COLUMN_NAME_REMOTE_PARTY_NAME)
                .append(TEXT_TYPE)
                .append(NOT_NULL)
                .append(COMMA_SEP)
                .append(ChatThreadDb.COLUMN_NAME_PROFILE_PIC)
                .append(TEXT_TYPE)
                .append(");");
        return sb.toString();
    }

    public String dropTable() {
        return "DROP TABLE IF EXISTS " + ChatThreadDb.TABLE_NAME;
    }


    @Override
    public void saveChatThreadDetails(@NonNull Context context, @NonNull ChatThread chatThread) {
        // Toast.makeText(MyApp.getContext(), chatThread.getFrom() + "      " + chatThread.getCurrentThreadId(), Toast.LENGTH_SHORT).show();
        if (!TextUtils.isEmpty(chatThread.getCurrentThreadId())) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ChatThreadDb._ID, chatThread.getFrom());
            contentValues.put(ChatThreadDb.COLUMN_NAME_THREAD_ID, chatThread.getCurrentThreadId());
            contentValues.put(ChatThreadDb.COLUMN_NAME_CATEGORY_ID, chatThread
                    .getCurrentCategoryId());

            contentValues.put(ChatThreadDb.COLUMN_NAME_REMOTE_PARTY_NAME, chatThread.getFromName());
            contentValues.put(ChatThreadDb.COLUMN_NAME_PROFILE_PIC, chatThread.getProfilePic());

            getContentResolver(context).insert(LocalDbProvider.URI_CHAT_THREADS, contentValues);
        }
    }

    @Override
    public ArrayList<ChatThread> getAllChatThreads(@NonNull Context context) {
        return null;
    }


    @Override
    public ChatThread getChatThreadDetails(@NonNull Context context, @NonNull String remoteParty) {
        ChatThread thread = null;

        Cursor cursor = getContentResolver(context).query(LocalDbProvider.URI_CHAT_THREADS,
                new String[]{
                        ChatThreadDb._ID,
                        ChatThreadDb.COLUMN_NAME_THREAD_ID,
                        ChatThreadDb.COLUMN_NAME_CATEGORY_ID,
                        ChatThreadDb.COLUMN_NAME_REMOTE_PARTY_NAME,
                        ChatThreadDb.COLUMN_NAME_PROFILE_PIC
                }, ChatThreadDb._ID + "=?", new String[]{remoteParty}, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    do {
                        thread = new ChatThread(remoteParty, cursor.getString(cursor.getColumnIndex
                                (ChatThreadDb.COLUMN_NAME_REMOTE_PARTY_NAME)));
                        thread.setProfilePic(cursor.getString(cursor.getColumnIndex(ChatThreadDb
                                .COLUMN_NAME_PROFILE_PIC)));
                        thread.setCurrentThreadId(cursor.getString(cursor.getColumnIndex
                                (ChatThreadDb.COLUMN_NAME_THREAD_ID)));
                        thread.setCurrentCategoryId(cursor.getString(cursor.getColumnIndex
                                (ChatThreadDb.COLUMN_NAME_CATEGORY_ID)));
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }

        return thread;
    }

    /*      @Override
          public ArrayList<ChatThread> getAllChatThreads(@NonNull Context context) {
              ArrayList<ChatThread> threads = null;

              Cursor cursor = getContentResolver(context).query(LocalDbProvider.URI_CHAT_THREADS, null,
                      null, null, null);
              if (cursor != null) {
                  try {
                      if (cursor.moveToFirst()) {
                          threads = new ArrayList<>();
                          do {
                              String remoteParty = cursor.getString(cursor.getColumnIndex
                                      (ChatThreadDb._ID));
                              String remotePartyName = cursor.getString(cursor.getColumnIndex
                                      (ChatThreadDb.COLUMN_NAME_REMOTE_PARTY_NAME));
                              String threadId = cursor.getString(cursor.getColumnIndex(ChatThreadDb
                                      .COLUMN_NAME_THREAD_ID));
                              String categoryId = cursor.getString(cursor.getColumnIndex(ChatThreadDb
                                      .COLUMN_NAME_CATEGORY_ID));
                              ChatThread thread = new ChatThread(remoteParty, remotePartyName);
                              thread.setProfilePic(cursor.getString(cursor.getColumnIndex(ChatThreadDb
                                      .COLUMN_NAME_PROFILE_PIC)));
                              thread.setCurrentThreadId(threadId);
                              thread.setCurrentCategoryId(categoryId);

                              ChatMessagesDaoImpl messagesDao = ChatMessagesDaoImpl.getInstance();
                              ChatMessage message = messagesDao.getLastReceivedChat(context, remoteParty);
                              int unreadCount = messagesDao.getAllUnreadChatsCount(context, remoteParty);
                              ArrayList<ChatMessage> messages = new ArrayList<>();
                              messages.add(message);
                              thread.setMessages(messages);
                              thread.setUnreadCount(unreadCount);
                              threads.add(thread);
                          } while (cursor.moveToNext());
                      }
                  } finally {
                      cursor.close();
                  }
              }

              return threads;
          }
      */
    @Override
    public void deleteAllChatThreads(@NonNull Context context) {
        ChatMessagesDaoImpl.getInstance().deleteAllChats(context);
        getContentResolver(context).delete(LocalDbProvider.URI_CHAT_THREADS, null, null);
    }

    @Override
    public void setNewThreadId(@NonNull Context context, @NonNull String remoteParty,
                               @NonNull String threadId) {
        if (!TextUtils.isEmpty(threadId)) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ChatThreadDb.COLUMN_NAME_THREAD_ID, threadId);

            getContentResolver(context).update(LocalDbProvider.URI_CHAT_THREADS, contentValues,
                    ChatThreadDb._ID + "=?", new String[]{remoteParty});
        }
    }

    @Override
    public void setNewCategoryId(@NonNull Context context, @NonNull String remoteParty,
                                 @NonNull String categoryId) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatThreadDb.COLUMN_NAME_CATEGORY_ID, categoryId);

        getContentResolver(context).update(LocalDbProvider.URI_CHAT_THREADS, contentValues,
                ChatThreadDb._ID + "=?", new String[]{remoteParty});
    }
}
