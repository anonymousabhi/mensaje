package com.example.smacklibrary;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by android on 2/25/2016.
 */
public abstract class NotificationDao extends BaseDao {
/*    public abstract void saveNotification(@NonNull Context context,
                                          @NonNull final NotificationData notificationData);*/

    public abstract int getAllUnreadNotificationCount(@NonNull Context context);

    public abstract void markNotificationAsRead(@NonNull Context context,
                                                @NonNull final String remoteParty,
                                                @NonNull final String type);

    public abstract void deleteAllNotification(@NonNull Context context);

    public abstract void markAllChatNotificationAsRead(@NonNull Context context,
                                                       @NonNull final String remoteParty);
}
