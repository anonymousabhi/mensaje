package com.example.smacklibrary;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseHelper extends SQLiteOpenHelper {
    static Context contexts;
    // Database
    public static final String SQL_DATABASE_NAME = "theLocalTribe.db";

    // We should specify mapping of all app versions to SQL_DATABASE_VERSION
    public static final int SQL_DATABASE_VERSION = 1;

    private static DatabaseHelper mInstance = null;

    private static DatabaseHelper getInstance(Context context) {
        contexts = MyApp.getContext();
        if (mInstance == null) {
            mInstance = new DatabaseHelper(contexts);
        }
        return mInstance;
    }

    public static SQLiteDatabase getDatabase(boolean writeable, Context context) {
        return writeable ? getInstance(context).getWritableDatabase() : getInstance(context)
                .getReadableDatabase();
    }

    private DatabaseHelper(Context context) {
        super(context, SQL_DATABASE_NAME, null, SQL_DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropTables(db);
        onCreate(db);
    }

    /*
     * Create all needed tables
     */
    private void createTables(SQLiteDatabase database) {
        database.execSQL(ChatThreadsDaoImpl.getInstance().getCreateTableSQL());
        database.execSQL(ChatMessagesDaoImpl.getInstance().getCreateTableSQL());
        database.execSQL(NotificationDaoImpl.getInstance().getCreateTableSQL());
    }

    private void dropTables(SQLiteDatabase database) {
        database.execSQL(ChatThreadsDaoImpl.getInstance().dropTable());
        database.execSQL(ChatMessagesDaoImpl.getInstance().dropTable());
        database.execSQL(NotificationDaoImpl.getInstance().dropTable());
    }
}
