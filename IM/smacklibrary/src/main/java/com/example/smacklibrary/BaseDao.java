package com.example.smacklibrary;


import android.content.ContentResolver;
import android.content.Context;

public abstract class BaseDao {
    public String TEXT_TYPE = " TEXT";
    public String INTEGER_TYPE = " INTEGER";
    public String BLOB_TYPE = " BLOB";
    public String DATETIME_TYPE = " DATETIME";
    public String BOOLEAN_TYPE = " BOOLEAN";

    public String COMMA_SEP = ", ";
    public String PRIMARY_KEY = " PRIMARY KEY";
    public String FOREIGN_KEY = " FOREIGN KEY";
    public String AUTO_INCREMENT = " AUTOINCREMENT";
    public String NOT_NULL = " NOT NULL";
    public String UNIQUE = " UNIQUE";
    public String REFERENCES = " REFERENCES ";

    public ContentResolver getContentResolver(Context context) {
        return context.getApplicationContext().getContentResolver();
    }
}

