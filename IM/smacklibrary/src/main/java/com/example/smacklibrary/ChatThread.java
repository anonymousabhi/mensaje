package com.example.smacklibrary;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ChatThread implements Parcelable {
    private String from;
    private String fromName;
    private String profilePic;
    private int unreadCount;
    private ArrayList<ChatMessage> messages;
    private String currentThreadId = "";
    private String currentCategoryId = "";

    private String NotificationToken;


    public ChatThread(String from, String fromName) {
        this.from = from;
        this.fromName = fromName;
    }

    public ArrayList<ChatMessage> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<ChatMessage> messages) {
        this.messages = messages;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getCurrentThreadId() {
        return currentThreadId;
    }

    public void setCurrentThreadId(String currentThreadId) {
        this.currentThreadId = currentThreadId;
    }

    public String getCurrentCategoryId() {
        return currentCategoryId;
    }

    public void setCurrentCategoryId(String currentCategoryId) {
        this.currentCategoryId = currentCategoryId;
    }

    public String getNotificationToken() {
        return NotificationToken;
    }

    public void setNotificationToken(String notificationToken) {
        NotificationToken = notificationToken;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(from);
        dest.writeString(fromName);
        dest.writeString(profilePic);
        dest.writeString(currentThreadId);
        dest.writeString(currentCategoryId);
        dest.writeInt(unreadCount);
        dest.writeTypedList(messages);
        dest.writeString(NotificationToken);
    }

    public static final Creator CREATOR = new Creator() {
        public ChatThread createFromParcel(Parcel in) {
            return new ChatThread(in);
        }

        public ChatThread[] newArray(int size) {
            return new ChatThread[size];
        }
    };

    private ChatThread(Parcel in) {
        from = in.readString();
        fromName = in.readString();
        profilePic = in.readString();
        currentThreadId = in.readString();
        currentCategoryId = in.readString();
        unreadCount = in.readInt();
        messages = new ArrayList<>();
        in.readTypedList(messages, ChatMessage.CREATOR);
        NotificationToken = in.readString();
    }
}
