package com.example.smacklibrary;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;


/*import com.thelocaltribe.ChatActivity;
import com.thelocaltribe.LandingPageActivity;
import com.thelocaltribe.R;
import com.thelocaltribe.apihelper.RemoteException;
import com.thelocaltribe.apihelper.RemoteHelper;
import com.thelocaltribe.apihelper.TltService;
import com.thelocaltribe.chat.SmackService;
import com.thelocaltribe.datatypes.Booking;
import com.thelocaltribe.datatypes.ChatBuddy;
import com.thelocaltribe.datatypes.ChatMessage;
import com.thelocaltribe.datatypes.ChatThread;
import com.thelocaltribe.datatypes.ChatUserDetails;
import com.thelocaltribe.datatypes.ChatUserDetailsRequest;
import com.thelocaltribe.datatypes.User;
import com.thelocaltribe.storage.daoImpl.ChatMessagesDaoImpl;
import com.thelocaltribe.storage.daoImpl.ChatThreadsDaoImpl;
*/
/*
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;*/

public class ChatUtility {
    private static final String TAG = ChatUtility.class.getSimpleName();
    private StorageUtil storageUtil;
    private static final int CHAT_NOTIFICATION_ID = 1001;
    private static final int MAX_ALLOWED_LINES = 7;

    private static ChatUtility mInstance;
    private Context mContext;
    private String mCurrentChatParty;
    private NotificationManager mNotificationManager;
    private Bitmap mDefaultLargeIconBitmap;
    private NotificationCompat.Builder mChatNotificationBuilder;
    private HashMap<String, ChatThread> mChatThreads = new HashMap<>();
    private HashMap<String, ArrayList<ChatMessage>> mTempChats = new HashMap<>();
    private HashMap<String, String> mTempThreadIds = new HashMap<>();
    private HashSet<String> mRosterRequests = new HashSet<>();
    private HashSet<String> mTempUsers = new HashSet<>();
    private HashSet<String> mBlockedUsersList = new HashSet<>();

    private boolean mChatNotificationPending;
    private ArrayList<ChatBuddy> rosterEntries;
    //   private UserInfoCallback mCurrentChatParryInfoCallback;

    /*    public void performCleanUpOnLogout() {
            deleteAllChats();

            mInstance = null;
            mContext = null;
            mCurrentChatParty = null;
            mNotificationManager = null;
            mDefaultLargeIconBitmap = null;
            mChatNotificationBuilder = null;
            mChatThreads = null;
            mTempChats = null;
            mTempThreadIds = null;
            mRosterRequests = null;
            mTempUsers = null;
            mBlockedUsersList = null;

            mChatNotificationPending = false;
            rosterEntries = null;
            mCurrentChatParryInfoCallback = null;
        }
    */
    private ChatUtility(Context context) {
        mContext = context.getApplicationContext();

        mNotificationManager = (NotificationManager) mContext.getSystemService(Context
                .NOTIFICATION_SERVICE);

        mDefaultLargeIconBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap
                .ic_launcher);
        storageUtil = new StorageUtil(context);
        mChatNotificationBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(mDefaultLargeIconBitmap)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setColor(context.getResources().getColor(R.color.colorAccent));
    }

    public static ChatUtility getInstance(@NonNull Context context) {
        if (mInstance == null) {
            mInstance = new ChatUtility(context);
        }

        return mInstance;
    }

    /*    public void setCurrentChatParticipant(String remoteParty) {
            mCurrentChatParty = remoteParty;
            if (!TextUtils.isEmpty(remoteParty)) {
                int updatedRowsCount = ChatMessagesDaoImpl.getInstance().markThreadAsRead(mContext,
                        remoteParty);

                if (updatedRowsCount > 0) {
                    // Issue an updated notification, if any
                    issueChatNotification();
                }
            }
        }
*/
    private void saveNewThreadId(String remoteParty, String threadId) {
        ChatThreadsDaoImpl.getInstance().setNewThreadId(mContext, remoteParty, threadId);
    }

    /*      public void saveNewCategoryId(String remoteParty, String categoryId) {
              ChatThreadsDaoImpl.getInstance().setNewCategoryId(mContext, remoteParty, categoryId);
          }

          public ArrayList<ChatThread> getAllChats() {
              return ChatThreadsDaoImpl.getInstance().getAllChatThreads(mContext);
          }

      public HashMap<String, ArrayList<ChatMessage>> getAllFailedChats() {
          HashMap<String, ArrayList<ChatMessage>> messageThreadMap = new HashMap<>();
          ArrayList<ChatMessage> failedMessages = ChatMessagesDaoImpl.getInstance()
                  .getAllFailedChats(mContext);
          if (failedMessages != null && failedMessages.size() > 0) {
              for (ChatMessage message : failedMessages) {
                  ChatThread chatThread = getUserFromLocalDb(message.getRemoteParty());
                  if (chatThread != null) {
                      ArrayList<ChatMessage> messages;
                      if (messageThreadMap.containsKey(chatThread.getCurrentThreadId())) {
                          messages = messageThreadMap.get(chatThread.getCurrentThreadId());
                      } else {
                          messages = new ArrayList<>();
                          messageThreadMap.put(chatThread.getCurrentThreadId(), messages);
                      }

                      messages.add(message);
                  }
              }
          }

          return messageThreadMap;
      }

      public boolean isConversationActive(String remoteParty) {
          boolean chatActive = false;
          ChatMessage message = ChatMessagesDaoImpl.getInstance().getLastReceivedChat(mContext,
                  remoteParty);
          if (message != null) {
              switch (message.getType()) {
                  case INVITE_ACCEPTED:
                  case TEXT:
                  case CART_OFFER:
                  case CART_UPDATE:
                  case ORDER:
                  case OFFER_EXPIRED:
                      chatActive = true;
                      break;
              }
          }

          return chatActive;
      }

      private void deleteAllChats() {
          ChatThreadsDaoImpl.getInstance().deleteAllChatThreads(mContext);
      }
  */
    public void onNewChatSend(ChatMessage message, String threadId) {
        // Set a unique message id as we want to track read status etc
        message.setMessageId(UUID.randomUUID().toString());

        ChatMessagesDaoImpl.getInstance().saveChatMessage(mContext, message);
        //ChatMessagesDaoImpl.getInstance().saveChatMessage(mContext, message);

        Intent intent = new Intent(SmackService.SEND_MESSAGE);
        intent.setPackage(mContext.getPackageName());
        intent.putExtra(SmackService.BUNDLE_MESSAGE, message);
        intent.putExtra(SmackService.BUNDLE_THREAD_ID, threadId);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }

        mContext.sendBroadcast(intent);
    }

    public void onChatInviteAccepted(ChatThread user, Boolean isAccept) {
        storageUtil.removeInvite(user.getFrom());
        Intent resIntent = new Intent(SmackService.ROSTER_RESPONSE);
        resIntent.setPackage(mContext.getPackageName());
        resIntent.putExtra(SmackService.BUNDLE_USER_ID, user.getFrom());
        resIntent.putExtra(SmackService.BUNDLE_IS_ACCEPT, isAccept);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            resIntent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }
        mContext.sendBroadcast(resIntent);
        if (isAccept) {
            if (getUserFromLocalDb(user.getFrom()) == null) {
                ChatThreadsDaoImpl.getInstance().saveChatThreadDetails(mContext, user);
            } else {
                saveNewThreadId(user.getFrom(), user.getCurrentThreadId());
                saveNewCategoryId(user.getFrom(), user.getCurrentCategoryId());
            }
        }
    }


    public void onAddUser(ChatThread user, boolean isAdd) {
        Intent intent = new Intent(SmackService.NEW_ROSTER);
        intent.setPackage(mContext.getPackageName());
        intent.putExtra(SmackService.BUNDLE_USER_ID, user.getFrom());
        intent.putExtra(SmackService.BUNDLE_IS_ADD, isAdd);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }

        mContext.sendBroadcast(intent);

        if (isAdd) {
            if (getUserFromLocalDb(user.getFrom()) == null) {
                ChatThreadsDaoImpl.getInstance().saveChatThreadDetails(mContext, user);
            } else {
                saveNewThreadId(user.getFrom(), user.getCurrentThreadId());
                saveNewCategoryId(user.getFrom(), user.getCurrentCategoryId());
            }
        }
    }

    public void saveNewCategoryId(String remoteParty, String categoryId) {
        ChatThreadsDaoImpl.getInstance().setNewCategoryId(mContext, remoteParty, categoryId);
    }
   /*

    public void onBlockUser(String userId, boolean isBlock) {
        Intent intent = new Intent(SmackService.BLOCK_USER);
        intent.setPackage(mContext.getPackageName());
        intent.putExtra(SmackService.BUNDLE_USER_ID, userId);
        intent.putExtra(SmackService.BUNDLE_IS_BLOCK, isBlock);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }

        mContext.sendBroadcast(intent);
    }*/

/*
    public void onChatStateChanged(SmackService.ChatStateType chatState, String threadId) {
        if (TextUtils.isEmpty(threadId)) {
            return;
        }

        Intent intent = new Intent(SmackService.CHAT_STATE_TX);
        intent.setPackage(mContext.getPackageName());
        intent.putExtra(SmackService.BUNDLE_CHAT_STATE, chatState);
        intent.putExtra(SmackService.BUNDLE_THREAD_ID, threadId);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
        }

        mContext.sendBroadcast(intent);
    }*/

/*
    private void saveChatTemporarily(ChatMessage message, String threadId) {
        String remoteParty = message.getRemoteParty();
        ArrayList<ChatMessage> messages = mTempChats.get(remoteParty);
        if (messages == null) {
            messages = new ArrayList<>();
            mTempChats.put(remoteParty, messages);
            mTempThreadIds.put(remoteParty, threadId);
        }

        messages.add(message);
    }*/

    /*  public void onChatSendStateChange(ChatMessage message, String threadId) {
          ChatMessagesDaoImpl.getInstance().changeMessageSendState(mContext, message);
          if (mCurrentChatParryInfoCallback != null) {
              mCurrentChatParryInfoCallback.onMessageSendStateChanged(message);
          }
      }
  */


    /*private void sendMessageWithSelfNotification(ChatMessage message, ChatThread user) {
        onNewChatSend(message, user.getCurrentThreadId());

        // Send the same chat to local receiver
        if (!TextUtils.isEmpty(mCurrentChatParty) && mCurrentChatParty.equals(user.getFrom()) &&
                mCurrentChatParryInfoCallback != null) {
            mCurrentChatParryInfoCallback.onNewChatReceived(message);
        }
    }

    public void sendBookingStatusMessage(String remoteParty, Booking.Status bookingStatus, String
            orderId) {
        ChatThread user = getUserFromLocalDb(remoteParty);
        if (user == null) {
            return;
        }

        ChatMessage.Type type;
        boolean sendCloseConversation = false;
        ChatMessage.JsonMessage jsonMessage = new ChatMessage.JsonMessage();
        switch (bookingStatus) {
            case OFFER_MADE:
                type = ChatMessage.Type.CART_OFFER;
                break;
            case OFFER_ASK_CHANGES:
                type = ChatMessage.Type.CART_UPDATE;
                break;
            case ORDER_PLACED:
                type = ChatMessage.Type.ORDER;
                break;
            case ORDER_CANCEL_BY_CUSTOMER:
            case ORDER_CANCEL_BY_MERCHANT:
                type = ChatMessage.Type.ORDER_CANCEL;
                sendCloseConversation = true;
                break;
            case OFFER_EXPIRED:
                type = ChatMessage.Type.OFFER_EXPIRED;
                sendCloseConversation = true;
                break;
            default:
                return;
        }

        jsonMessage.setType(ChatMessage.Type.getTypeValue(type));
        if (!TextUtils.isEmpty(orderId)) {
            jsonMessage.setOrderId(orderId);
        }

        ChatMessage message = new ChatMessage(user.getFrom(), null);
        message.setType(type);
        message.setJsonMessage(jsonMessage);
        message.setMessageDateTime(new Date());
        message.setIsUnread(false);
        sendMessageWithSelfNotification(message, user);

        if (sendCloseConversation) {
            type = ChatMessage.Type.CONVERSATION_CLOSED;
            jsonMessage = new ChatMessage.JsonMessage();
            jsonMessage.setType(ChatMessage.Type.getTypeValue(type));

            message = new ChatMessage(user.getFrom(), null);
            message.setType(type);
            message.setJsonMessage(jsonMessage);
            message.setMessageDateTime(new Date());
            message.setIsUnread(false);
            sendMessageWithSelfNotification(message, user);
        }
    }

    public void onChatSyncCompleted(ArrayList<ChatMessage> messages, HashMap<String, String>
            threadIds) {
        if (messages != null) {
            boolean canIssueNotification = true;
            for (ChatMessage message : messages) {
                String remoteParty = message.getRemoteParty();
                ChatThread userThread = getUserFromLocalDb(remoteParty);
                String threadId = threadIds.get(remoteParty);
                if (userThread != null) {
                    ChatMessagesDaoImpl.getInstance().saveChatMessage(mContext, message);
                    if (!userThread.getCurrentThreadId().equals(threadId)) {
                        userThread.setCurrentThreadId(threadId);
                        saveNewThreadId(message.getRemoteParty(), threadId);
                    }
                } else {
                    saveChatTemporarily(message, threadId);
                    mChatNotificationPending = true;
                    getUserFromRemote(remoteParty);
                    canIssueNotification = false;
                }
            }

            if (canIssueNotification) {
                issueChatNotification();

                if (mCurrentChatParryInfoCallback != null) {
                    mCurrentChatParryInfoCallback.onBulkChatUpdates(messages);
                }
            }
        }
    }
*/
    public ChatBuddy getBuddyData(String userId) {
        ChatBuddy buddyData = null;
        if (rosterEntries != null) {
            for (ChatBuddy entry : rosterEntries) {
                if (entry.getUserId().equals(userId)) {
                    buddyData = entry;
                    break;
                }
            }
        }

        return buddyData;
    }

    public void cancelRosterNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancel(CHAT_NOTIFICATION_ID);
        }
    }


    public boolean isUserBlocked(String userId) {
        return mBlockedUsersList.contains(userId);
    }
/*
    public void onRosterListUpdated(ArrayList<ChatBuddy> rosterEntries) {
        this.rosterEntries = rosterEntries;
        if (mCurrentChatParryInfoCallback != null && !TextUtils.isEmpty(mCurrentChatParty)) {
            ChatBuddy buddy = getBuddyData(mCurrentChatParty);
            if (buddy == null) {
                buddy = new ChatBuddy(mCurrentChatParty);
            }

            mCurrentChatParryInfoCallback.onUserStatusUpdate(buddy);
        }
    }*/

    /*   public void onRemoteChatStateChanged(String remoteParty, SmackService.ChatStateType state) {
           if (mCurrentChatParryInfoCallback != null && !TextUtils.isEmpty(mCurrentChatParty) &&
                   remoteParty.equals(mCurrentChatParty)) {
               mCurrentChatParryInfoCallback.onChatStateChanged(state);
           }
       }
   */
    public ChatThread getUserFromLocalDb(String remoteParty) {
        ChatThread user = mChatThreads.get(remoteParty);
        if (user == null) {
            ChatThread thread = ChatThreadsDaoImpl.getInstance().getChatThreadDetails(mContext,
                    remoteParty);
            if (thread != null) {
                mChatThreads.put(remoteParty, thread);
                user = thread;
            }
        }

        return user;
    }

    private void getUserFromRemote(String remoteParty) {
        if (!mTempUsers.contains(remoteParty)) {
            mTempUsers.add(remoteParty);
            //      callChatUserDetailsApi(new String[]{remoteParty});
        }
    }

    /*
      private void callChatUserDetailsApi(String[] remoteParties) {
          ChatUserDetailsRequest chatUser = new ChatUserDetailsRequest();
          chatUser.addParam(ChatUserDetailsRequest.NAME);
          chatUser.addParam(ChatUserDetailsRequest.PROFILE_PIC);
          chatUser.addParam(ChatUserDetailsRequest.NOTIFICATION_TOKEN);
          for (String remoteParty : remoteParties) {
              chatUser.addUserId(remoteParty);
          }

          try {
              TltService service = RemoteHelper.getService(TltService.class);
              Call<ChatUserDetails> mChatUserDetailsCall = service.getChatUserDetails(chatUser);
              mChatUserDetailsCall.enqueue(new Callback<ChatUserDetails>() {
                  @Override
                  public void onResponse(Call<ChatUserDetails> call, Response<ChatUserDetails>
                          response) {
                      if (response.isSuccessful()) {
                          handleChatUserDetails(response.body());
                      } else {
                          Log.e(TAG, response.code() + " " + response.message());
                      }
                  }

                  @Override
                  public void onFailure(Call<ChatUserDetails> call, Throwable t) {
                      Log.e(TAG, TextUtils.isEmpty(t.getMessage()) ? "API Failed" : t.getMessage());
                      t.printStackTrace();
                  }
              });
          } catch (RemoteException.NotInitializedException | RemoteException.ServiceException e) {
              e.printStackTrace();
          }
      }
*/
  /*    private void handleChatUserDetails(ChatUserDetails resultObj) {
          if (resultObj.getResponseCode().equals("200")) {
              ArrayList<User> users = resultObj.getUser();
              if (users != null && users.size() > 0) {
                  for (User user : users) {
                      ChatThread thread = new ChatThread(user.get_id().get$id(), user
                              .getFullName());
                      thread.setProfilePic(user.getProfilePicture());
                      thread.setNotificationToken(user.getNotificationToken());
                      if (mTempThreadIds.containsKey(thread.getFrom())) {
                          thread.setCurrentThreadId(mTempThreadIds.get(thread.getFrom()));
                          mTempThreadIds.remove(thread.getFrom());
                      }

                      ChatThreadsDaoImpl.getInstance().saveChatThreadDetails(mContext,
                              thread);
                      mChatThreads.put(thread.getFrom(), thread);
                      if (mTempChats.containsKey(thread.getFrom())) {
                          ArrayList<ChatMessage> messages = mTempChats.get(thread.getFrom());
                          if (messages != null) {
                              ChatMessagesDaoImpl.getInstance().saveChatMessages(mContext,
                                      messages);
                          }

                          mTempChats.remove(thread.getFrom());
                          if (mCurrentChatParryInfoCallback != null) {
                              mCurrentChatParryInfoCallback.onBulkChatUpdates(messages);
                          }
                      }
                  }

                  if (mChatNotificationPending) {
                      issueChatNotification();

                      if (mTempChats.size() == 0) {
                          mChatNotificationPending = false;
                      }
                  }
              }
          }
      }
*/


}
