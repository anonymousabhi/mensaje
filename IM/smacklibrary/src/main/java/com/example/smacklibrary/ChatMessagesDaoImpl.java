package com.example.smacklibrary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.example.smacklibrary.DatabaseContract.ChatMessageDb;
import com.example.smacklibrary.DatabaseContract.ChatThreadDb;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/*import com.thelocaltribe.providers.LocalDbProvider;
import com.thelocaltribe.storage.DatabaseContract.ChatMessageDb;
import com.thelocaltribe.storage.DatabaseContract.ChatThreadDb;
import com.thelocaltribe.util.AppUtil;*/

public class ChatMessagesDaoImpl extends ChatMessagesDao {

    private static ChatMessagesDaoImpl mInstance;

    private ChatMessagesDaoImpl() {

    }

    public static ChatMessagesDaoImpl getInstance() {
        if (mInstance == null) {
            mInstance = new ChatMessagesDaoImpl();
        }

        return mInstance;
    }

    public String getCreateTableSQL() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append(ChatMessageDb.TABLE_NAME)
                .append(" (")
                .append(ChatMessageDb._ID)
                .append(INTEGER_TYPE)
                .append(PRIMARY_KEY)
                .append(AUTO_INCREMENT)
                .append(COMMA_SEP)
                .append(ChatMessageDb.COLUMN_NAME_MESSAGE)
                .append(TEXT_TYPE)
                .append(NOT_NULL)
                .append(COMMA_SEP)
                .append(ChatMessageDb.COLUMN_NAME_MESSAGE_ID)
                .append(TEXT_TYPE)
                .append(UNIQUE)
                .append(NOT_NULL)
                .append(COMMA_SEP)
                .append(ChatMessageDb.COLUMN_NAME_MESSAGE_TIME)
                .append(DATETIME_TYPE)
                .append(NOT_NULL)
                .append(COMMA_SEP)
                .append(ChatMessageDb.COLUMN_NAME_IS_TX)
                .append(BOOLEAN_TYPE)
                .append(COMMA_SEP)
                .append(ChatMessageDb.COLUMN_NAME_IS_UNREAD)
                .append(BOOLEAN_TYPE)
                .append(COMMA_SEP)
                .append(ChatMessageDb.COLUMN_NAME_REMOTE_PARTY)
                .append(TEXT_TYPE)
                .append(NOT_NULL)
                .append(COMMA_SEP)
                .append(ChatMessageDb.COLUMN_NAME_MESSAGE_TYPE)
                .append(TEXT_TYPE)
                .append(NOT_NULL)
                .append(COMMA_SEP)
                .append(ChatMessageDb.COLUMN_NAME_MESSAGE_JSON)
                .append(TEXT_TYPE)
                .append(COMMA_SEP)
                .append(ChatMessageDb.COLUMN_NAME_SEND_STATE)
                .append(TEXT_TYPE)
                .append(NOT_NULL)
                .append(COMMA_SEP)
                .append(FOREIGN_KEY)
                .append(" (")
                .append(ChatMessageDb.COLUMN_NAME_REMOTE_PARTY)
                .append(")")
                .append(REFERENCES)
                .append(ChatThreadDb.TABLE_NAME)
                .append("(")
                .append(ChatThreadDb._ID)
                .append(")")
                .append(");");
        return sb.toString();
    }

    public String dropTable() {
        return "DROP TABLE IF EXISTS " + ChatMessageDb.TABLE_NAME;
    }

    private String getDateTime(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.
                getDefault());
        return dateFormat.format(date);
    }

    private Date parseDate(String dateTime) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.
                getDefault());
        try {
            return dateFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void saveChatMessage(@NonNull Context context, @NonNull ChatMessage message) {
        Gson gson = new Gson();
        // Toast.makeText(context, "In saveChatMessage", Toast.LENGTH_SHORT).show();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatMessageDb.COLUMN_NAME_REMOTE_PARTY, message.getRemoteParty());
        contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE, message.getMessage());
        contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE_ID, message.getMessageId());
        contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE_TIME, getDateTime(message
                .getMessageDateTime()));
        //  Toast.makeText(MyApp.getContext(), message.isTx() + "", Toast.LENGTH_SHORT).show();
        contentValues.put(ChatMessageDb.COLUMN_NAME_IS_TX, message.isTx());
        contentValues.put(ChatMessageDb.COLUMN_NAME_IS_UNREAD, message.isUnread());
        contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE_TYPE, message.getType().name());
        contentValues.put(ChatMessageDb.COLUMN_NAME_SEND_STATE, message.getSendState().name());
        ChatMessage.JsonMessage jsonMessage = message.getJsonMessage();
        if (jsonMessage != null) {
            contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE_JSON, gson.toJson(message
                    .getJsonMessage()));
        } else {
            contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE_JSON, "");
        }

        getContentResolver(context).insert(LocalDbProvider.URI_CHAT_MESSAGES, contentValues);
        //  Toast.makeText(context, "Message saved", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void saveChatMessages(@NonNull Context context, @NonNull ArrayList<ChatMessage> messages) {

    }


    @Override
    public ArrayList<ChatMessage> getAllChats(@NonNull Context context, @NonNull String remoteParty, Date time) {
        return null;
    }

    @Override
    public ChatMessage getLastReceivedChat(@NonNull Context context, @NonNull String remoteParty) {
        return null;
    }

    @Override
    public int getAllUnreadChatsCount(@NonNull Context context, @NonNull String from) {
        return 0;
    }

    @Override
    public int markThreadAsRead(@NonNull Context context, @NonNull String remoteParty) {
        return 0;
    }

    @Override
    public void deleteAllChats(@NonNull Context context) {

    }

    @Override
    public void changeMessageSendState(@NonNull Context context, @NonNull ChatMessage message) {

    }

    @Override
    public ArrayList<ChatMessage> getAllFailedChats(@NonNull Context context) {
        return null;
    }


    /*@Override
    public void saveChatMessages(@NonNull Context context, @NonNull ArrayList<ChatMessage>
            messages) {
        int msgSize = messages.size();
        if (msgSize > 0) {
            ContentValues[] values = new ContentValues[msgSize];
            Gson gson = new Gson();
            for (int i = 0; i < msgSize; i++) {
                ChatMessage message = messages.get(i);
                ContentValues contentValues = new ContentValues();
                contentValues.put(ChatMessageDb.COLUMN_NAME_REMOTE_PARTY, message.getRemoteParty());
                contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE, message.getMessage());
                contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE_ID, message.getMessageId());
                contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE_TIME, getDateTime(message
                        .getMessageDateTime()));
                contentValues.put(ChatMessageDb.COLUMN_NAME_IS_TX, message.isTx());
                contentValues.put(ChatMessageDb.COLUMN_NAME_IS_UNREAD, message.isUnread());
                contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE_TYPE, message.getType().name());
                contentValues.put(ChatMessageDb.COLUMN_NAME_SEND_STATE, message.getSendState()
                        .name());
                ChatMessage.JsonMessage jsonMessage = message.getJsonMessage();
                if (jsonMessage != null) {
                    contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE_JSON, gson.toJson(message
                            .getJsonMessage()));
                } else {
                    contentValues.put(ChatMessageDb.COLUMN_NAME_MESSAGE_JSON, "");
                }

                values[i] = contentValues;
            }

            getContentResolver(context).bulkInsert(LocalDbProvider.URI_CHAT_MESSAGES, values);
        }
    }
*/
    /*
    private ChatMessage getDateMessage(String remoteParty, Date messageDate) {
        ChatMessage message = new ChatMessage(remoteParty, "");
        message.setMessageId("");
        message.setMessageDateTime(messageDate);
        message.setType(ChatMessage.Type.DATE);
        message.setIsUnread(false);

        return message;
    }
*/
    @Override
    public ArrayList<ChatMessage> getAllChats(@NonNull Context context, @NonNull String from) {
        ArrayList<ChatMessage> messages = null;

        Cursor cursor = getContentResolver(context).query(LocalDbProvider.URI_CHAT_MESSAGES, null,
                ChatMessageDb.COLUMN_NAME_REMOTE_PARTY + "=?", new String[]{from}, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    Gson gson = new Gson();
                    messages = new ArrayList<>();
                    do {
                        String remoteParty = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_REMOTE_PARTY));
                        String messageStr = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE));
                        String messageId = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE_ID));
                        String messageTime = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE_TIME));
                        String isTx = cursor.getString(cursor.getColumnIndex(ChatMessageDb.
                                COLUMN_NAME_IS_TX));
                        String isUnread = cursor.getString(cursor.getColumnIndex(ChatMessageDb.
                                COLUMN_NAME_IS_UNREAD));
                        String type = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_MESSAGE_TYPE));
                        String json = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_MESSAGE_JSON));
                        String sendState = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_SEND_STATE));

                        /*Date messageDate = parseDate(messageTime);
                        if (messageDate != null) {
                            if (messages.size() == 0) {
                                messages.add(getDateMessage(remoteParty, messageDate));
                            } else {
                                ChatMessage lastMessage = messages.get(messages.size() - 1);
                                if (AppUtil.areSameDates(messageDate, lastMessage
                                        .getMessageDateTime()) != 0) {
                                    messages.add(getDateMessage(remoteParty, messageDate));
                                }
                            }
                        }*/

                        ChatMessage message = new ChatMessage(remoteParty, messageStr);
                        message.setMessageId(messageId);
                        message.setIsTx(isTx.equals("1"));
                        message.setIsUnread(isUnread.equals("1"));
                        //message.setMessageDateTime(messageDate);
                        message.setType(ChatMessage.Type.valueOf(type));
                        message.setSendState(ChatMessage.SendState.valueOf(sendState));
                        if (!TextUtils.isEmpty(json)) {
                            message.setJsonMessage(gson.fromJson(json, ChatMessage.JsonMessage
                                    .class));
                        }

                        messages.add(message);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }

        return messages;
    }

    /*
        @Override
        public ArrayList<ChatMessage> getAllChats(@NonNull Context context, @NonNull String from,
                                                  Date lastMessageTime) {
            ArrayList<ChatMessage> messages = null;
            if (lastMessageTime == null) {
                return null;
            }

            String selection = "DATETIME(" + ChatMessageDb.COLUMN_NAME_MESSAGE_TIME + ") > DATETIME" +
                    "(?) AND " + ChatMessageDb.COLUMN_NAME_REMOTE_PARTY + " = ?";
            String[] selectionArgs = new String[]{getDateTime(lastMessageTime), from};

            Cursor cursor = getContentResolver(context).query(LocalDbProvider.URI_CHAT_MESSAGES, null,
                    selection, selectionArgs, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        Gson gson = AppUtil.getCustomGson();
                        messages = new ArrayList<>();
                        do {
                            String remoteParty = cursor.getString(cursor.getColumnIndex
                                    (ChatMessageDb.COLUMN_NAME_REMOTE_PARTY));
                            String messageStr = cursor.getString(cursor.getColumnIndex
                                    (ChatMessageDb.COLUMN_NAME_MESSAGE));
                            String messageId = cursor.getString(cursor.getColumnIndex
                                    (ChatMessageDb.COLUMN_NAME_MESSAGE_ID));
                            String messageTime = cursor.getString(cursor.getColumnIndex
                                    (ChatMessageDb.COLUMN_NAME_MESSAGE_TIME));
                            String isTx = cursor.getString(cursor.getColumnIndex(ChatMessageDb.
                                    COLUMN_NAME_IS_TX));
                            String isUnread = cursor.getString(cursor.getColumnIndex(ChatMessageDb.
                                    COLUMN_NAME_IS_UNREAD));
                            String type = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                    .COLUMN_NAME_MESSAGE_TYPE));
                            String json = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                    .COLUMN_NAME_MESSAGE_JSON));
                            String sendState = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                    .COLUMN_NAME_SEND_STATE));

                            Date messageDate = parseDate(messageTime);
                            if (messageDate != null) {
                                if (messages.size() == 0) {
                                    if (AppUtil.areSameDates(messageDate, lastMessageTime) != 0) {
                                        messages.add(getDateMessage(remoteParty, messageDate));
                                    }
                                } else {
                                    ChatMessage lastMessage = messages.get(messages.size() - 1);
                                    if (AppUtil.areSameDates(messageDate, lastMessage
                                            .getMessageDateTime()) != 0) {
                                        messages.add(getDateMessage(remoteParty, messageDate));
                                    }
                                }
                            }

                            ChatMessage message = new ChatMessage(remoteParty, messageStr);
                            message.setMessageId(messageId);
                            message.setIsTx(isTx.equals("1"));
                            message.setIsUnread(isUnread.equals("1"));
                            message.setMessageDateTime(messageDate);
                            message.setType(ChatMessage.Type.valueOf(type));
                            message.setSendState(ChatMessage.SendState.valueOf(sendState));
                            if (!TextUtils.isEmpty(json)) {
                                message.setJsonMessage(gson.fromJson(json, ChatMessage.JsonMessage
                                        .class));
                            }

                            messages.add(message);
                        } while (cursor.moveToNext());
                    }
                } finally {
                    cursor.close();
                }
            }

            return messages;
        }

        public ChatMessage getLastReceivedChat(@NonNull Context context, @NonNull String from) {
            ChatMessage message = null;

            Cursor cursor = getContentResolver(context).query(LocalDbProvider.URI_LAST_CHAT_MESSAGE,
                    null, ChatMessageDb.COLUMN_NAME_REMOTE_PARTY + "=?", new String[]{from},
                    ChatMessageDb._ID + " DESC");
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        Gson gson = AppUtil.getCustomGson();
                        do {
                            String remoteParty = cursor.getString(cursor.getColumnIndex
                                    (ChatMessageDb.COLUMN_NAME_REMOTE_PARTY));
                            String messageStr = cursor.getString(cursor.getColumnIndex
                                    (ChatMessageDb.COLUMN_NAME_MESSAGE));
                            String messageId = cursor.getString(cursor.getColumnIndex
                                    (ChatMessageDb.COLUMN_NAME_MESSAGE_ID));
                            String messageTime = cursor.getString(cursor.getColumnIndex
                                    (ChatMessageDb.COLUMN_NAME_MESSAGE_TIME));
                            String isTx = cursor.getString(cursor.getColumnIndex(ChatMessageDb.
                                    COLUMN_NAME_IS_TX));
                            String isUnread = cursor.getString(cursor.getColumnIndex(ChatMessageDb.
                                    COLUMN_NAME_IS_UNREAD));
                            String type = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                    .COLUMN_NAME_MESSAGE_TYPE));
                            String json = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                    .COLUMN_NAME_MESSAGE_JSON));
                            String sendState = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                    .COLUMN_NAME_SEND_STATE));

                            Date messageDate = parseDate(messageTime);

                            message = new ChatMessage(remoteParty, messageStr);
                            message.setMessageId(messageId);
                            message.setIsTx(isTx.equals("1"));
                            message.setIsUnread(isUnread.equals("1"));
                            message.setMessageDateTime(messageDate);
                            message.setType(ChatMessage.Type.valueOf(type));
                            message.setSendState(ChatMessage.SendState.valueOf(sendState));
                            if (!TextUtils.isEmpty(json)) {
                                message.setJsonMessage(gson.fromJson(json, ChatMessage.JsonMessage
                                        .class));
                            }
                        } while (cursor.moveToNext());
                    }
                } finally {
                    cursor.close();
                }
            }

            return message;
        }
    */
    public ChatMessage getNewMessage(@NonNull Context context) {
        ChatMessage message = null;

        Cursor cursor = getContentResolver(context).query(LocalDbProvider.URI_CHAT_MESSAGES, null,
                ChatMessageDb.COLUMN_NAME_IS_UNREAD + "=?", new String[]{"1"}, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    Gson gson = new Gson();

                    do {
                        String remoteParty = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_REMOTE_PARTY));
                        String messageStr = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE));
                        String messageId = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE_ID));
                        String type = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_MESSAGE_TYPE));
                        String json = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_MESSAGE_JSON));
                        String sendState = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_SEND_STATE));
                        String isTx = cursor.getString(cursor.getColumnIndex(ChatMessageDb.
                                COLUMN_NAME_IS_TX));
                        message = new ChatMessage(remoteParty, messageStr);
                        message.setMessageId(messageId);
                        //boolean b = Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(ChatMessageDb.COLUMN_NAME_IS_TX)));
                        // Toast.makeText(MyApp.getContext(), b + "", Toast.LENGTH_SHORT).show();
                        message.setIsTx(isTx.equals("1"));
                        message.setMessageDateTime(parseDate(cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE_TIME))));
                        message.setType(ChatMessage.Type.valueOf(type));
                        message.setSendState(ChatMessage.SendState.valueOf(sendState));

                        if (!TextUtils.isEmpty(json)) {
                            message.setJsonMessage(gson.fromJson(json, ChatMessage.JsonMessage
                                    .class));
                        }
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }

        return message;
    }

    @Override
    public ArrayList<ChatMessage> getAllUnreadChats(@NonNull Context context) {
        ArrayList<ChatMessage> messages = null;

        Cursor cursor = getContentResolver(context).query(LocalDbProvider.URI_CHAT_MESSAGES, null,
                ChatMessageDb.COLUMN_NAME_IS_UNREAD + "=?", new String[]{"1"}, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    Gson gson = new Gson();
                    messages = new ArrayList<>();
                    do {
                        String remoteParty = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_REMOTE_PARTY));
                        String messageStr = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE));
                        String messageId = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE_ID));
                        String type = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_MESSAGE_TYPE));
                        String json = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_MESSAGE_JSON));
                        String sendState = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_SEND_STATE));

                        ChatMessage message = new ChatMessage(remoteParty, messageStr);
                        message.setMessageId(messageId);
                        message.setIsTx(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_IS_TX))));
                        message.setMessageDateTime(parseDate(cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE_TIME))));
                        message.setType(ChatMessage.Type.valueOf(type));
                        message.setSendState(ChatMessage.SendState.valueOf(sendState));

                        if (!TextUtils.isEmpty(json)) {
                            message.setJsonMessage(gson.fromJson(json, ChatMessage.JsonMessage
                                    .class));
                        }
                        messages.add(message);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }

        return messages;
    }

  /*  @Override
    public int getAllUnreadChatsCount(@NonNull Context context, @NonNull String from) {
        int unreadCount = 0;
        String selection = ChatMessageDb.COLUMN_NAME_REMOTE_PARTY + "=? AND " + ChatMessageDb
                .COLUMN_NAME_IS_UNREAD + "=?";
        Cursor cursor = getContentResolver(context).query(LocalDbProvider.URI_CHAT_MESSAGES, null,
                selection, new String[]{from, "1"}, null);
        if (cursor != null) {
            try {
                unreadCount = cursor.getCount();
            } finally {
                cursor.close();
            }
        }

        return unreadCount;
    }

    @Override
    public int markThreadAsRead(@NonNull Context context, @NonNull final String remoteParty) {
        int unreadCount = getAllUnreadChatsCount(context, remoteParty);
        if (unreadCount > 0) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(ChatMessageDb.COLUMN_NAME_IS_UNREAD, false);

            getContentResolver(context).update(LocalDbProvider.URI_CHAT_MESSAGES, contentValues,
                    ChatMessageDb.COLUMN_NAME_REMOTE_PARTY + "=?", new String[]{remoteParty});
        }

        return unreadCount;
    }

    @Override
    public void deleteAllChats(@NonNull Context context) {
        getContentResolver(context).delete(LocalDbProvider.URI_CHAT_MESSAGES, null, null);
    }

    @Override
    public void changeMessageSendState(@NonNull Context context, @NonNull ChatMessage message) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ChatMessageDb.COLUMN_NAME_SEND_STATE, message.getSendState().name());

        getContentResolver(context).update(LocalDbProvider.URI_CHAT_MESSAGES, contentValues,
                ChatMessageDb.COLUMN_NAME_MESSAGE_ID + "=?", new String[]{message.getMessageId()});
    }

    @Override
    public ArrayList<ChatMessage> getAllFailedChats(@NonNull Context context) {
        ArrayList<ChatMessage> messages = null;

        Cursor cursor = getContentResolver(context).query(LocalDbProvider.URI_CHAT_MESSAGES, null,
                ChatMessageDb.COLUMN_NAME_SEND_STATE + "=?", new String[]{ChatMessage.SendState
                        .FAILED.name()}, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    Gson gson = AppUtil.getCustomGson();
                    messages = new ArrayList<>();
                    do {
                        String remoteParty = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_REMOTE_PARTY));
                        String messageStr = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE));
                        String messageId = cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE_ID));
                        String type = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_MESSAGE_TYPE));
                        String json = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_MESSAGE_JSON));
                        String sendState = cursor.getString(cursor.getColumnIndex(ChatMessageDb
                                .COLUMN_NAME_SEND_STATE));

                        ChatMessage message = new ChatMessage(remoteParty, messageStr);
                        message.setMessageId(messageId);
                        message.setIsTx(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_IS_TX))));
                        message.setMessageDateTime(parseDate(cursor.getString(cursor.getColumnIndex
                                (ChatMessageDb.COLUMN_NAME_MESSAGE_TIME))));
                        message.setType(ChatMessage.Type.valueOf(type));
                        message.setSendState(ChatMessage.SendState.valueOf(sendState));

                        if (!TextUtils.isEmpty(json)) {
                            message.setJsonMessage(gson.fromJson(json, ChatMessage.JsonMessage
                                    .class));
                        }
                        messages.add(message);
                    } while (cursor.moveToNext());
                }
            } finally {
                cursor.close();
            }
        }

        return messages;
    }*/
}
