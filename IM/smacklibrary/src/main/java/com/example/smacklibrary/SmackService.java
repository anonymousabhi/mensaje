package com.example.smacklibrary;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.sasl.SASLError;
import org.jivesoftware.smack.sasl.SASLErrorException;
import org.jivesoftware.smack.sasl.packet.SaslStreamElements;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

//import com.thelocaltribe.datatypes.User;
//import com.thelocaltribe.receivers.ChatReceiver;
//import com.thelocaltribe.storage.StorageUtil;

public class SmackService extends Service {
    public static final String NEW_MESSAGE = "com.tlt.rx.msg";
    public static final String OFFLINE_MESSAGES = "com.tlt.offline.msg";
    public static final String MESSAGE_SYNC = "com.tlt.msg.sync";
    public static final String ROSTER_LIST = "com.tlt.roster.list";
    public static final String INCOMING_ROSTER = "com.tlt.rx.roster";
    public static final String BLOCKED_USERS_LIST = "com.tlt.blocked.users";
    public static final String CHAT_STATE_RX = "com.tlt.chat_state.rx";
    public static final String MESSAGE_SEND_STATUS = "com.tlt.msg.send.status";
    public static final String CONNECTION_STATE_CHANGED = "com.tlt.conn.state.changed";

    /**
     * All the intents that are handled by {@link SmackConnection's BroadcastReceiver}
     */
    public static final String NEW_ROSTER = "com.tlt.tx.roster";
    public static final String SEND_MESSAGE = "com.tlt.tx.msg";
    public static final String BLOCK_USER = "com.tlt.block.user";
    public static final String ROSTER_RESPONSE = "com.tlt.roster.res";
    public static final String CHAT_STATE_TX = "com.tlt.chat.state.tx";
    public static final String SEND_FAILED_MESSAGES = "com.tlt.send.failed.msg";
    public static final String INVITE_PACKET = "com.tlt.invite.packet";
    public static final String INCOMING_ROSTER_REQUEST = "com.tlt.invite.process";
    public static final String INVITE_ACCEPTED = "com.tlt.invite.accepted";
    public static final String INVITE_REJECTED = "com.tlt.invite.rejected";
    /**
     * Different extra identifiers used by either of the above BroadcastReceivers
     */
    public static final String BUNDLE_MESSAGE = "b_message";
    public static final String BUNDLE_USER_ID = "b_block_user_id";
    public static final String BUNDLE_IS_BLOCK = "b_is_block";
    public static final String BUNDLE_IS_ADD = "b_is_add";
    public static final String BUNDLE_IS_ACCEPT = "b_is_accept";
    public static final String BUNDLE_MSG_SYNC = "b_msg_sync";
    public static final String BUNDLE_ROSTER = "b_roster";
    public static final String BUNDLE_THREAD_ID = "b_thread_id";
    public static final String BUNDLE_CHAT_STATE = "b_chat_state";
    public static final String BUNDLE_CONNECTION_STATE = "b_connection_state";
    public static final String BUNDLE_CLOSE_TIME = "b_close_time";
    public static final String PRESENCE_TYPE = "p_info";
    public static final String PRESENCE_FROM = "p_from";
    StorageUtil storageUtil;
    String email, password, name, activity;
    User user;


    public enum ChatStateType {
        TYPING,
        PAUSED,
        ACTIVE,
        INACTIVE,
        GONE
    }

    public enum ConnectionState {
        CONNECTED, CONNECTING, RECONNECTING, DISCONNECTED, CLOSED_ON_ERROR, RECONNECTED
    }

    public static ConnectionState sConnectionState;
    private boolean doChatSync = false;
    HashMap<String, String> user_data;

    public static ConnectionState getState() {
        if (sConnectionState == null) {
            return ConnectionState.DISCONNECTED;
        }

        return sConnectionState;
    }

    private boolean mActive;
    private Thread mThread;
    private Handler mTHandler;
    private SmackConnection mConnection;
    private ChatUtility mchatUtility;

    private final MyAIDL.Stub mBinder = new MyAIDL.Stub() {
        @Override
        public void onAddUser(String user, boolean isAdd) {
            user = User.get_id(user);
            ChatThread thread = new ChatThread(user, user);
            thread.setCurrentThreadId(user);
            mchatUtility.onAddUser(thread, true);
        }

        @Override
        public void isAccepted(String user, boolean isAdd) {
            ChatThread thread = new ChatThread(User.get_id(user), User.get_id(user));
            thread.setCurrentThreadId(User.get_id(user));
            mchatUtility.onChatInviteAccepted(thread, true);
        }

        public void onNewChatSend(String peer, String msg) {
            ChatMessage chat = new ChatMessage(peer, msg);
            chat.setMessageDateTime(new Date());
            ChatMessage.JsonMessage jsonMessage = new ChatMessage.JsonMessage();
            ChatMessage.Type type = ChatMessage.Type.TEXT;
            jsonMessage.setType(ChatMessage.Type.getTypeValue(type));
            chat.setJsonMessage(jsonMessage);
            mchatUtility.onNewChatSend(chat, "@abhi");
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mchatUtility = ChatUtility.getInstance(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        user_data = new HashMap<>();
        storageUtil = new StorageUtil(getApplicationContext());
        if (intent != null && intent.hasExtra(BUNDLE_MSG_SYNC)) {
            doChatSync = intent.getBooleanExtra(BUNDLE_MSG_SYNC, false);
        }
        user = storageUtil.getUser();
        if (user != null) {
            user_data.put("email", user.getEmail());
            user_data.put("password", user.getPassword());
            user_data.put("name", user.getName());
            activity = "StoredUser";
        } else if (intent != null) {
            email = intent.getExtras().getString("email");
            password = intent.getExtras().getString("password");
            name = intent.getExtras().getString("name");
            activity = intent.getExtras().getString("activity");
            user_data.put("email", email);
            user_data.put("password", password);
            user_data.put("name", name);
        }

        start(user_data);

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stop();
    }

    public void start(HashMap<String, String> user_data) {
        if (!mActive) {
            email = user_data.get("email");
            password = user_data.get("password");
            name = user_data.get("name");
            final User savedUser = new User(email, password, name);
            // final User savedUser =StorageUtil.getCachedUser();
            if (savedUser == null) {
                return;
            }

            mActive = true;

            // Create ConnectionThread Loop
            if (mThread == null || !mThread.isAlive()) {
                mThread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        Looper.prepare();
                        mTHandler = new Handler();
                        initConnection(savedUser);
                        Looper.loop();
                    }

                });

                mThread.start();
            }
        }
    }

    public void stop() {
        mActive = false;
        mTHandler.post(new Runnable() {

            @Override
            public void run() {
                if (mConnection != null) {
                    mConnection.disconnect();
                }
            }
        });
    }

    void Loggedin() {
        if (activity.equals("MainActivity")) {
            storageUtil.addUser(email, password, name);
            CallBack.getInstance(this).startNewActivity();
           /* Intent intent = new Intent(SmackService.INVITE);
            sendBroadcast(intent);*/
        }
    }

    private void initConnection(User savedUser) {
        String serverUrl = "69.64.33.244";
        if (mConnection == null && savedUser != null) {
            mConnection = new SmackConnection(this, savedUser, serverUrl);
        }

        try {
            mConnection.connect();
        } catch (XMPPException | SmackException | IOException e) {
            e.printStackTrace();
           /* Toast.makeText(this, "Couldn't connect to the chat server\n" + e.getMessage(),
                    Toast.LENGTH_LONG).show();*/
            stop();
            return;
        }

        try {
            mConnection.login(doChatSync);
            Loggedin();
        } catch (XMPPException | SmackException | IOException e) {
            e.printStackTrace();
            if (e instanceof SASLErrorException) {
                SaslStreamElements.SASLFailure failure = ((SASLErrorException) e).getSASLFailure();
                if (failure.getSASLError() == SASLError.not_authorized) {
                /*    Toast.makeText(this, "User doesn't exist on the chat server, creating a new " +
                            "user...", Toast.LENGTH_SHORT).show();*/

                    // First disconnect the already established connection with the server
                    mConnection.disconnect();

                    // Then create a new smack connection instance
                    mConnection = new SmackConnection(this, savedUser, serverUrl);

                    // Now connect with the server and create a new user, then try logging in
                    try {
                        mConnection.connect();
                        mConnection.createUser();
                        mConnection.login(doChatSync);
                        Loggedin();
                    } catch (IOException | XMPPException | SmackException e1) {
                        e1.printStackTrace();
                        stop();
                    }
                }
            }
        }
    }
}
