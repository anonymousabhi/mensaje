package com.example.smacklibrary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;


public class NotificationDaoImpl extends NotificationDao {
    private static NotificationDaoImpl mInstance;

    private NotificationDaoImpl() {

    }

    public static NotificationDaoImpl getInstance() {
        if (mInstance == null) {
            mInstance = new NotificationDaoImpl();
        }

        return mInstance;
    }

    public String getCreateTableSQL() {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS ")
                .append(DatabaseContract.NotificationCount.TABLE_NAME)
                .append(" (")
                .append(DatabaseContract.NotificationCount._ID)
                .append(TEXT_TYPE)
                .append(PRIMARY_KEY)
                .append(COMMA_SEP)
                .append(DatabaseContract.NotificationCount.COLUMN_NAME_REMOTE_USER_ID)
                .append(TEXT_TYPE)
                .append(NOT_NULL)
                .append(COMMA_SEP)
                .append(DatabaseContract.NotificationCount.COLUMN_NAME_NOTIFICATION_TYPE)
                .append(TEXT_TYPE)
                .append(COMMA_SEP)
                .append(DatabaseContract.NotificationCount.COLUMN_NAME_READ_STATUS)
                .append(BOOLEAN_TYPE)
                .append(NOT_NULL)
                .append(");");
        return sb.toString();
    }

    public String dropTable() {
        return "DROP TABLE IF EXISTS " + DatabaseContract.NotificationCount.TABLE_NAME;
    }
/*
    @Override
    public void saveNotification(@NonNull Context context, @NonNull NotificationData notificationData) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseContract.NotificationCount.COLUMN_NAME_REMOTE_USER_ID, notificationData.getRemoteUsreId());
        contentValues.put(DatabaseContract.NotificationCount.COLUMN_NAME_NOTIFICATION_TYPE, notificationData.getType());
        contentValues.put(DatabaseContract.NotificationCount.COLUMN_NAME_READ_STATUS, false);

        getContentResolver(context).insert(LocalDbProvider.URI_NOTIFICATION, contentValues);
    }*/

    @Override
    public int getAllUnreadNotificationCount(@NonNull Context context) {
        int notiCount = 0;
        Cursor cursor = getContentResolver(context).query(LocalDbProvider.URI_NOTIFICATION, null,
                DatabaseContract.NotificationCount.COLUMN_NAME_READ_STATUS + "=?", new String[]{"0"}, null);
        if (cursor != null) {
            notiCount = cursor.getCount();
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        return notiCount;

    }

    @Override
    public void markNotificationAsRead(@NonNull Context context, @NonNull String remoteParty, @NonNull String type) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseContract.NotificationCount.COLUMN_NAME_READ_STATUS, true);

        getContentResolver(context).update(LocalDbProvider.URI_NOTIFICATION, contentValues,
                DatabaseContract.NotificationCount.COLUMN_NAME_REMOTE_USER_ID + "=? AND "
                        + DatabaseContract.NotificationCount.COLUMN_NAME_REMOTE_USER_ID + "=? ", new String[]{remoteParty, type});
    }

    @Override
    public void markAllChatNotificationAsRead(@NonNull Context context, @NonNull String remoteParty) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseContract.NotificationCount.COLUMN_NAME_READ_STATUS, true);

        getContentResolver(context).update(LocalDbProvider.URI_NOTIFICATION, contentValues,
                DatabaseContract.NotificationCount.COLUMN_NAME_REMOTE_USER_ID + "=? ", new String[]{remoteParty});
    }


    @Override
    public void deleteAllNotification(@NonNull Context context) {
        getContentResolver(context).delete(LocalDbProvider.URI_NOTIFICATION, null, null);

    }


}
