package com.example.smacklibrary;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.util.Map;


/**
 * Created by abhishek on 3/6/16.
 */
public class StorageUtil {
    public static final String USERSTATUSPREFERENCE = "status";
    public static final String MyPREFERENCES = "login";
    public static final String SAVEINVITATIONPREFERENCE = "invite";
    public static final String SAVEADDEDUSER = "added_user";
    public static final String REMOVE = "remove";

    Context context;
    SharedPreferences sharedPreferencesUserStatus, sharedpreferences, invitepreference, saveaddeduserprefs;
    SharedPreferences.Editor editor, editorStatus, editorInvite, editorSaveUser;
    String mEmail, mPassword, mName;
    int mUSerStatus;

    public StorageUtil(Context context) {
        this.context = context;
        saveaddeduserprefs = context.getSharedPreferences(SAVEADDEDUSER, context.MODE_PRIVATE);
        sharedPreferencesUserStatus = context.getSharedPreferences(USERSTATUSPREFERENCE, context.MODE_PRIVATE);
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, context.MODE_PRIVATE);
        invitepreference = context.getSharedPreferences(SAVEINVITATIONPREFERENCE, context.MODE_PRIVATE);
        editorSaveUser = saveaddeduserprefs.edit();
        editorInvite = invitepreference.edit();
        editor = sharedpreferences.edit();
        editorStatus = sharedPreferencesUserStatus.edit();
    }

    public void setAddUser(String from) {
        editorSaveUser.putString("user name:-" + from, from);
        editorSaveUser.apply();
    }

    public void setInvitepreference(String from) {
        Toast.makeText(context, from + " sends you an invite!", Toast.LENGTH_SHORT).show();
        editorInvite.putString("user name:-" + from, from);
        editorInvite.apply();
    }

    public Map<String, ?> getAllAddedUSer() {
        return saveaddeduserprefs.getAll();
    }


    public void removeInvite(String reject_user) {
        editorInvite.remove("user name:-" + reject_user);
        editorInvite.apply();
    }

    public Map<String, ?> getAllInvitation() {
        return invitepreference.getAll();
    }

    // if i=1 Invitation_Activity else if i=2 Chatting_Activity
    public void setUserStatus(int i) {
        editorStatus.putInt("status", i);
        editorStatus.apply();
    }

    public int getUserStatus() {
        mUSerStatus = sharedPreferencesUserStatus.getInt("status", 0);
        return mUSerStatus;
    }


    public User getUser() {
        mEmail = sharedpreferences.getString("email", null);
        mPassword = sharedpreferences.getString("password", null);
        mName = sharedpreferences.getString("name", null);
        if (mEmail != null && mPassword != null && mName != null) {
            //Toast.makeText(MyApp.getContext(), mEmail + "  " + mPassword + "  " + mName, Toast.LENGTH_SHORT).show();
            return new User(mEmail, mPassword, mName);
        } else {
            //Toast.makeText(MyApp.getContext(), "NULL", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    public void addUser(String email, String password, String name) {
        editor.putString("email", email);
        editor.putString("password", password);
        editor.putString("name", name);
        //editor.putBoolean("isLogin", user.getLoggein());
        //Toast.makeText(MyApp.getContext(), user.get_id() + "  " + user.getEmail() + "  " + user.getPassword(), Toast.LENGTH_SHORT).show();
        editor.apply();
    }


    public void clear() {
        editor.clear();
        editorStatus.clear();
        editorInvite.clear();
        editorSaveUser.clear();
        editorSaveUser.apply();
        editorInvite.apply();
        editorStatus.apply();
        editor.apply();
        Toast.makeText(context, "Logout succesful", Toast.LENGTH_SHORT).show();
    }
}

