package com.example.smacklibrary;

import android.content.Context;
import android.support.annotation.NonNull;

import java.util.ArrayList;

public abstract class ChatThreadsDao extends BaseDao {
   /* public abstract void saveChatThreadDetails(@NonNull Context context,
                                               @NonNull final ChatThread chatThread);*/

    public abstract ChatThread getChatThreadDetails(@NonNull Context context,
                                                    @NonNull final String remoteParty);

    public abstract void saveChatThreadDetails(@NonNull Context context,
                                               @NonNull final ChatThread chatThread);

    public abstract ArrayList<ChatThread> getAllChatThreads(@NonNull Context context);

    public abstract void deleteAllChatThreads(@NonNull Context context);

    public abstract void setNewThreadId(@NonNull Context context,
                                        @NonNull final String remoteParty,
                                        @NonNull final String threadId);

    public abstract void setNewCategoryId(@NonNull Context context,
                                          @NonNull final String remoteParty,
                                          @NonNull final String categoryId);
}
