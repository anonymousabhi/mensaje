package com.example.smacklibrary;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by abhishek on 27/7/16.
 */
public class ChatReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case SmackService.INVITE_PACKET:
                String from = intent.getStringExtra(SmackService.PRESENCE_FROM);
                CallBack.getInstance(context).sendInvite(from);
                break;
            case SmackService.INVITE_ACCEPTED:
                String user_name = intent.getStringExtra("user");
                CallBack.getInstance(context).saveAddedUser(user_name);
                break;
            case SmackService.INVITE_REJECTED:
                Toast.makeText(context, "Chat invite rejected!", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
