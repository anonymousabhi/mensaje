package com.example.smacklibrary;

import android.app.Application;
import android.content.Context;

/**
 * Created by abhishek on 7/6/16.
 */
public class MyApp extends Application {
    private static Context mContext;

    public static Context getContext() {
        //  return instance.getApplicationContext();
        return mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //  instance = this;
        mContext = getApplicationContext();
    }
}
