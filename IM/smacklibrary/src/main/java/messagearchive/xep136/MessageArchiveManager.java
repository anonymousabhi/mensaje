package messagearchive.xep136;

import messagearchive.xep136.packet.ArchiveCollection;
import messagearchive.xep136.packet.ArchiveCollectionItem;
import messagearchive.xep136.packet.ArchiveCollectionResult;
import messagearchive.xep136.packet.ArchiveList;
import messagearchive.xep136.packet.ArchiveListItem;
import messagearchive.xep136.packet.MamPrefIQ;
import messagearchive.xep136.provider.ArchiveCollectionResultProvider;
import messagearchive.xep136.provider.ArchivedListProvider;

import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPConnectionRegistry;
import org.jivesoftware.smack.XMPPException.XMPPErrorException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.rsm.packet.RSMSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

public class MessageArchiveManager extends Manager {
    public static final String NAMESPACE = MessageArchiveConstants.NAMESPACE;
    private static final Map<XMPPConnection, MessageArchiveManager> INSTANCES = new WeakHashMap<>();

    static {
        XMPPConnectionRegistry.addConnectionCreationListener(new ConnectionCreationListener() {
            @Override
            public void connectionCreated(XMPPConnection connection) {
                getInstanceFor(connection);
            }
        });
    }

    private MessageArchiveManager(XMPPConnection connection) {
        super(connection);
        ServiceDiscoveryManager.getInstanceFor(connection).addFeature(NAMESPACE);

        ProviderManager.addIQProvider(ArchiveList.ELEMENT, ArchiveList.NAMESPACE,
                new ArchivedListProvider());
        ProviderManager.addIQProvider(ArchiveCollectionResult.ELEMENT, ArchiveCollectionResult.
                NAMESPACE, new ArchiveCollectionResultProvider());
    }

    public static synchronized MessageArchiveManager getInstanceFor(XMPPConnection connection) {
        MessageArchiveManager maManager = INSTANCES.get(connection);
        if (maManager == null) {
            maManager = new MessageArchiveManager(connection);
            INSTANCES.put(connection, maManager);
        }

        return maManager;
    }

    public List<ArchiveListItem> retrieveArchivedLists(Date start, Date end, String withJid) throws
            NoResponseException, XMPPErrorException, NotConnectedException {
        List<ArchiveListItem> listItems = new ArrayList<>();
        XMPPConnection connection = connection();
        RSMSet rsmRes = null;
        int totalCount = -1, retrievedCount = 0;

        while (true) {
            ArchiveList archivedListRequest = new ArchiveList(start, end, withJid);
            archivedListRequest.setType(IQ.Type.get);
            RSMSet rsmReq = rsmRes == null ? new RSMSet(MessageArchiveConstants.PAGE_SIZE) : new
                    RSMSet(MessageArchiveConstants.PAGE_SIZE, rsmRes.getLast(), RSMSet.
                    PageDirection.after);
            archivedListRequest.addExtension(rsmReq);

            PacketCollector resultCollector = connection.createPacketCollectorAndSend
                    (archivedListRequest);
            ArchiveList archivedListResult = resultCollector.nextResultOrThrow();
            rsmRes = archivedListResult.getExtension(RSMSet.ELEMENT, RSMSet.NAMESPACE);
            if (totalCount == -1) totalCount = rsmRes.getCount();
            retrievedCount += MessageArchiveConstants.PAGE_SIZE;

            listItems.addAll(archivedListResult.getListItems());
            if (retrievedCount >= totalCount) {
                break;
            }
        }

        return listItems;
    }

    public ArrayList<ArchiveCollectionItem> retrieveArchivedMessages
            (List<ArchiveListItem> listItems) throws NotConnectedException, XMPPErrorException,
            NoResponseException {
        ArrayList<ArchiveCollectionItem> archivedMessages = new ArrayList<>();
        XMPPConnection connection = connection();
        for (ArchiveListItem listItem : listItems) {
            RSMSet rsmRes = null;
            int totalCount = -1, retrievedCount = 0;
            while (true) {
                ArchiveCollection archivedCollectionRequest = new ArchiveCollection(listItem
                        .getStart(), listItem.getWith());
                archivedCollectionRequest.setType(IQ.Type.get);
                RSMSet rsmReq = rsmRes == null ? new RSMSet(MessageArchiveConstants.PAGE_SIZE) :
                        new RSMSet(MessageArchiveConstants.PAGE_SIZE, rsmRes.getLast(), RSMSet.
                                PageDirection.after);
                archivedCollectionRequest.addExtension(rsmReq);

                PacketCollector resultCollector = connection.createPacketCollectorAndSend
                        (archivedCollectionRequest);
                ArchiveCollectionResult archivedCollectionResult = resultCollector
                        .nextResultOrThrow();
                rsmRes = archivedCollectionResult.getExtension(RSMSet.ELEMENT, RSMSet.NAMESPACE);
                if (totalCount == -1) totalCount = rsmRes.getCount();
                retrievedCount += MessageArchiveConstants.PAGE_SIZE;

                archivedMessages.addAll(archivedCollectionResult.getMessages());
                if (retrievedCount >= totalCount) {
                    break;
                }
            }
        }

        return archivedMessages;
    }

    public ArrayList<ArchiveCollectionItem> getArchivedMessages() {
        try {
            List<ArchiveListItem> listItems = retrieveArchivedLists(null, null, null);
            return retrieveArchivedMessages(listItems);
        } catch (NoResponseException | XMPPErrorException | NotConnectedException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void getPreferences() throws NoResponseException, XMPPErrorException,
            NotConnectedException {
        MamPrefIQ mamPrefIQ = new MamPrefIQ();
        mamPrefIQ.setType(IQ.Type.get);

        connection().createPacketCollectorAndSend(mamPrefIQ).nextResultOrThrow();
    }
}