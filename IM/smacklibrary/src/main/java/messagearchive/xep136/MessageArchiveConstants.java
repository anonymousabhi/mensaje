package messagearchive.xep136;

public class MessageArchiveConstants {
    /* Namespace for Message Archive feature as per XEP-136 */
    public static final String NAMESPACE = "urn:xmpp:archive";

    /* Various ELEMENT names used in the documentation */
    public static final String LIST = "list";
    public static final String RETRIEVE = "retrieve";
    public static final String CHAT = "chat";
    public static final String SET = "set";
    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String BODY = "body";

    /* Various Attributes used in the documentation */
    public static final String START = "start";
    public static final String WITH = "with";
    public static final String END = "end";
    public static final String SUBJECT = "subject";
    public static final String VERSION = "version";
    public static final String JID = "jid";
    public static final String SECS = "secs";

    /* Other constants */
    public static final int PAGE_SIZE = 30;
}
