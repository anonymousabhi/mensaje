package messagearchive.xep136.provider;

import messagearchive.xep136.MessageArchiveConstants;
import messagearchive.xep136.packet.ArchiveList;
import messagearchive.xep136.packet.ArchiveListItem;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.rsm.packet.RSMSet;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class ArchivedListProvider extends IQProvider<ArchiveList> {

    private static ArchiveListItem parseChat(XmlPullParser parser) {
        String with = parser.getAttributeValue("", MessageArchiveConstants.WITH);
        String start = parser.getAttributeValue("", MessageArchiveConstants.START);

        return new ArchiveListItem(with, start);
    }

    @Override
    public ArchiveList parse(XmlPullParser parser, int initialDepth)
            throws XmlPullParserException, IOException, SmackException {
        ArchiveList list = new ArchiveList();
        boolean done = false;
        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG) {
                String name = parser.getName();
                if (name.equals(MessageArchiveConstants.CHAT)) {
                    list.addListItem(parseChat(parser));
                } else if (name.equals(MessageArchiveConstants.SET)) {
                    RSMSet extension = (RSMSet) PacketParserUtils.parseExtensionElement
                            (RSMSet.ELEMENT, RSMSet.NAMESPACE, parser);
                    list.addExtension(extension);
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getDepth() == initialDepth) {
                    done = true;
                }
            }
        }

        return list;
    }
}
