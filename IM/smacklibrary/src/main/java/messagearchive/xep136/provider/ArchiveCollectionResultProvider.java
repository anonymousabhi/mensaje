package messagearchive.xep136.provider;

import android.text.TextUtils;

import messagearchive.xep136.MessageArchiveConstants;
import messagearchive.xep136.packet.ArchiveCollectionItem;
import messagearchive.xep136.packet.ArchiveCollectionResult;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.provider.IQProvider;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.rsm.packet.RSMSet;
import org.jxmpp.util.XmppDateTime;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

public class ArchiveCollectionResultProvider extends IQProvider<ArchiveCollectionResult> {

    private static ArchiveCollectionResult parseChat(XmlPullParser parser) {
        String with = parser.getAttributeValue("", MessageArchiveConstants.WITH);
        String start = parser.getAttributeValue("", MessageArchiveConstants.START);
        String subject = parser.getAttributeValue("", MessageArchiveConstants.SUBJECT);
        String version = parser.getAttributeValue("", MessageArchiveConstants.VERSION);

        return new ArchiveCollectionResult(start, with, subject, version);
    }

    private static ArchiveCollectionItem parseCollectionItem(XmlPullParser parser, Date timeBase,
                                                             boolean from, String with) {
        int secs = Integer.parseInt(parser.getAttributeValue("", MessageArchiveConstants.SECS));
        String jid = parser.getAttributeValue("", MessageArchiveConstants.JID);
        if (TextUtils.isEmpty(jid)) {
            jid = with;
        }

        return new ArchiveCollectionItem(jid, timeBase, secs, from);
    }

    @Override
    public ArchiveCollectionResult parse(XmlPullParser parser, int initialDepth)
            throws XmlPullParserException, IOException, SmackException {
        ArchiveCollectionResult collectionResult = parseChat(parser);
        Date timeBase = null;
        try {
            timeBase = XmppDateTime.parseXEP0082Date(collectionResult.getStart());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        ArchiveCollectionItem item = null;
        boolean done = false;
        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG) {
                String name = parser.getName();
                switch (name) {
                    case MessageArchiveConstants.SET:
                        RSMSet extension = (RSMSet) PacketParserUtils.parseExtensionElement
                                (RSMSet.ELEMENT, RSMSet.NAMESPACE, parser);
                        collectionResult.addExtension(extension);
                        break;
                    case MessageArchiveConstants.FROM:
                        item = parseCollectionItem(parser, timeBase, true, collectionResult
                                .getWith());
                        break;
                    case MessageArchiveConstants.TO:
                        item = parseCollectionItem(parser, timeBase, false, collectionResult
                                .getWith());
                        break;
                    case MessageArchiveConstants.BODY:
                        if (item != null) {
                            item.setBody(parser.nextText());
                            collectionResult.addMessage(item);
                        }
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getDepth() == initialDepth) {
                    done = true;
                }
            }
        }

        return collectionResult;
    }
}
