package messagearchive.xep136.packet;

import messagearchive.xep136.MessageArchiveConstants;

import org.jivesoftware.smack.packet.IQ;
import org.jxmpp.util.XmppDateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ArchiveList extends IQ {

    public static final String ELEMENT = MessageArchiveConstants.LIST;
    public static final String NAMESPACE = MessageArchiveConstants.NAMESPACE;

    private Date start;
    private Date end;
    private String with;
    private List<ArchiveListItem> itemLists = new ArrayList<>();

    public ArchiveList() {
        this(null, null, null);
    }

    public ArchiveList(Date start, Date end, String with) {
        super(ELEMENT, NAMESPACE);

        this.start = start;
        this.end = end;
        this.with = with;
    }

    public void addListItem(ArchiveListItem item) {
        itemLists.add(item);
    }

    public List<ArchiveListItem> getListItems() {
        return itemLists;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(
            IQChildElementXmlStringBuilder xml) {
        if (start != null) {
            xml.optAttribute(MessageArchiveConstants.START, XmppDateTime.formatXEP0082Date(start));
        }
        if (end != null) {
            xml.optAttribute(MessageArchiveConstants.END, XmppDateTime.formatXEP0082Date(end));
        }
        xml.optAttribute(MessageArchiveConstants.WITH, with);
        xml.rightAngleBracket();
        return xml;
    }
}
