package messagearchive.xep136.packet;

import org.jivesoftware.smack.packet.IQ;

import messagearchive.xep136.MessageArchiveConstants;

public class ArchiveCollection extends IQ {

    public static final String ELEMENT = MessageArchiveConstants.RETRIEVE;
    public static final String NAMESPACE = MessageArchiveConstants.NAMESPACE;

    private String start;
    private String with;

    public ArchiveCollection() {
        this(null, null);
    }

    public ArchiveCollection(String start, String with) {
        super(ELEMENT, NAMESPACE);

        this.start = start;
        this.with = with;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(
            IQChildElementXmlStringBuilder xml) {
        xml.optAttribute(MessageArchiveConstants.START, start);
        xml.optAttribute(MessageArchiveConstants.WITH, with);
        xml.rightAngleBracket();
        return xml;
    }
}
