package messagearchive.xep136.packet;

import messagearchive.xep136.MessageArchiveConstants;

import org.jivesoftware.smack.packet.IQ;

import java.util.ArrayList;
import java.util.List;

public class ArchiveCollectionResult extends IQ {

    public static final String ELEMENT = MessageArchiveConstants.CHAT;
    public static final String NAMESPACE = MessageArchiveConstants.NAMESPACE;

    private String start;
    private String with;
    private String subject;
    private String version;
    private List<ArchiveCollectionItem> collectionItems = new ArrayList<>();

    public ArchiveCollectionResult(String start, String with, String subject, String version) {
        super(ELEMENT, NAMESPACE);

        this.start = start;
        this.with = with;
        this.subject = subject;
        this.version = version;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getWith() {
        return with;
    }

    public void setWith(String with) {
        this.with = with;
    }

    public void addMessage(ArchiveCollectionItem item) {
        this.collectionItems.add(item);
    }

    public List<ArchiveCollectionItem> getMessages() {
        return collectionItems;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder
            (IQChildElementXmlStringBuilder xml) {
        /*if (start != null) {
            xml.optAttribute(MessageArchiveConstants.START, XmppDateTime.formatXEP0082Date(start));
        }*/
        xml.optAttribute(MessageArchiveConstants.START, start);
        xml.optAttribute(MessageArchiveConstants.WITH, with);
        xml.optAttribute(MessageArchiveConstants.SUBJECT, subject);
        xml.optAttribute(MessageArchiveConstants.VERSION, version);
        xml.rightAngleBracket();
        for (ArchiveCollectionItem item : collectionItems) {
            xml.append(item.toXML());
        }
        return xml;
    }
}
