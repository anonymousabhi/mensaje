package messagearchive.xep136.packet;

import org.jivesoftware.smack.packet.IQ;

import messagearchive.xep136.MessageArchiveConstants;

public class MamPrefIQ extends IQ {

    public static final String ELEMENT = "pref";
    public static final String NAMESPACE = MessageArchiveConstants.NAMESPACE;

    public MamPrefIQ() {
        super(ELEMENT, NAMESPACE);
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(
            IQChildElementXmlStringBuilder xml) {
        xml.setEmptyElement();
        return xml;
    }
}
