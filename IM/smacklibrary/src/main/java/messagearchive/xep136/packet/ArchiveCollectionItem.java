package messagearchive.xep136.packet;

import java.util.Date;

public class ArchiveCollectionItem {
    private int secs;
    private String jid;
    private String body;
    private boolean from;
    private Date timeBase;

    public ArchiveCollectionItem(String jid, Date timeBase, int secs, boolean from) {
        this.jid = jid;
        this.secs = secs;
        this.timeBase = timeBase;
        this.from = from;
    }

    public int getSecs() {
        return secs;
    }

    public String getJid() {
        return jid;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isFrom() {
        return from;
    }

    public Date getMessageTime() {
        return new Date(timeBase.getTime() + secs * 1000);
    }

    public String toXML() {
        StringBuilder buf = new StringBuilder();
        buf.append(isFrom() ? "<from" : "<to");
        buf.append(" secs=\"").append(getSecs()).append("\"");
        if (getJid() != null) {
            buf.append(" jid=\"").append(getJid()).append("\"");
        }

        buf.append("><body>").append(getBody()).append("</body>");
        buf.append(isFrom() ? "</from>" : "</to>");
        return buf.toString();
    }
}
