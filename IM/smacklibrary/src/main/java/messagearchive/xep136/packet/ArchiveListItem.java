package messagearchive.xep136.packet;

public class ArchiveListItem {

    private final String with;
    private final String start;

    public ArchiveListItem(String with, String start) {
        this.with = with;
        this.start = start;
    }

    public String getWith() {
        return with;
    }

    public String getStart() {
        return start;
    }
}
