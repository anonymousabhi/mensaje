package messagearchive.xep313;

import android.text.TextUtils;

import org.jivesoftware.smack.ConnectionCreationListener;
import org.jivesoftware.smack.Manager;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackException.NoResponseException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPConnectionRegistry;
import org.jivesoftware.smack.XMPPException.XMPPErrorException;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.forward.packet.Forwarded;
import org.jivesoftware.smackx.rsm.packet.RSMSet;
import org.jivesoftware.smackx.xdata.FormField;
import org.jivesoftware.smackx.xdata.packet.DataForm;
import org.jxmpp.util.XmppDateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;

import messagearchive.xep313.filter.MamMessageFinFilter;
import messagearchive.xep313.filter.MamMessageResultFilter;
import messagearchive.xep313.packet.MamPacket.MamFinExtension;
import messagearchive.xep313.packet.MamPacket.MamResultExtension;
import messagearchive.xep313.packet.MamQueryIQ;
import messagearchive.xep313.provider.MamFinProvider;
import messagearchive.xep313.provider.MamResultProvider;

public class MessageArchiveManager extends Manager {
    public static final String NAMESPACE = Constants.NAMESPACE;
    private static final Map<XMPPConnection, MessageArchiveManager> INSTANCES = new WeakHashMap<>();

    static {
        XMPPConnectionRegistry.addConnectionCreationListener(new ConnectionCreationListener() {
            @Override
            public void connectionCreated(XMPPConnection connection) {
                getInstanceFor(connection);
            }
        });
    }

    private MessageArchiveManager(XMPPConnection connection) {
        super(connection);
        ServiceDiscoveryManager.getInstanceFor(connection).addFeature(NAMESPACE);

        ProviderManager.addExtensionProvider(Constants.FIN, NAMESPACE, new MamFinProvider());
        ProviderManager.addExtensionProvider(Constants.RESULT, NAMESPACE, new MamResultProvider());
    }

    public static synchronized MessageArchiveManager getInstanceFor(XMPPConnection connection) {
        MessageArchiveManager maManager = INSTANCES.get(connection);
        if (maManager == null) {
            maManager = new MessageArchiveManager(connection);
            INSTANCES.put(connection, maManager);
        }

        return maManager;
    }

    private static DataForm getNewMamForm() {
        FormField field = new FormField(FormField.FORM_TYPE);
        field.setType(FormField.Type.hidden);
        field.addValue(NAMESPACE);

        DataForm form = new DataForm(DataForm.Type.submit);
        form.addField(field);

        return form;
    }

    private MamQueryIqResult queryArchive(MamQueryIQ mamQueryIQ) throws NotConnectedException,
            XMPPErrorException, NoResponseException {
        XMPPConnection connection = connection();
        PacketCollector resultCollector = connection.createPacketCollectorAndSend(mamQueryIQ);
        PacketCollector finMessageCollector = connection.createPacketCollector(new
                MamMessageFinFilter(mamQueryIQ));
        PacketCollector.Configuration messageCollectorConfiguration = PacketCollector.
                newConfiguration().setStanzaFilter(new MamMessageResultFilter(mamQueryIQ))
                .setCollectorToReset(resultCollector);
        PacketCollector messageCollector = connection.createPacketCollector
                (messageCollectorConfiguration);

        List<Forwarded> messages;
        MamFinExtension mamFinExtension;
        try {
            resultCollector.nextResultOrThrow();

            Message finMessage = finMessageCollector.nextResultOrThrow();
            mamFinExtension = MamFinExtension.from(finMessage);

            messages = new ArrayList<>(messageCollector.getCollectedCount());
            Message message;
            while ((message = messageCollector.pollResult()) != null) {
                MamResultExtension mamResultExtension = MamResultExtension.from(message);
                messages.add(mamResultExtension.getForwarded());
            }
        } finally {
            messageCollector.cancel();
            finMessageCollector.cancel();
        }

        return new MamQueryIqResult(messages, mamFinExtension, DataForm.from(mamQueryIQ));
    }

    private MamQueryIqResult queryArchive(Integer max, Date start, Date end, String withJid) throws
            NotConnectedException, XMPPErrorException, NoResponseException {
        DataForm dataForm = null;
        if (start != null || end != null || !TextUtils.isEmpty(withJid)) {
            dataForm = getNewMamForm();

            if (start != null) {
                FormField formField = new FormField(Constants.START);
                formField.addValue(XmppDateTime.formatXEP0082Date(start));
                dataForm.addField(formField);
            }

            if (end != null) {
                FormField formField = new FormField(Constants.END);
                formField.addValue(XmppDateTime.formatXEP0082Date(end));
                dataForm.addField(formField);
            }

            if (!TextUtils.isEmpty(withJid)) {
                FormField formField = new FormField(Constants.WITH);
                formField.addValue(withJid);
                dataForm.addField(formField);
            }
        }

        MamQueryIQ mamQueryIQ = new MamQueryIQ(UUID.randomUUID().toString(), dataForm);
        mamQueryIQ.setType(IQ.Type.set);

        if (max <= 0) {
            max = Constants.DEFAULT_PAGE_SIZE;
        }

        RSMSet rsmSet = new RSMSet(max);
        mamQueryIQ.addExtension(rsmSet);

        return queryArchive(mamQueryIQ);
    }

    private MamQueryIqResult getNextPage(MamQueryIqResult mamQueryIqResult, Integer max) throws
            NotConnectedException, XMPPErrorException, NoResponseException {
        RSMSet previousRsmSet = mamQueryIqResult.mamFin.getRSMSet();
        if (max <= 0) {
            max = Constants.DEFAULT_PAGE_SIZE;
        }

        RSMSet rsmSet = new RSMSet(max, previousRsmSet.getLast(), RSMSet.PageDirection.after);

        MamQueryIQ mamQueryIQ = new MamQueryIQ(UUID.randomUUID().toString(), mamQueryIqResult.form);
        mamQueryIQ.setType(IQ.Type.set);
        mamQueryIQ.addExtension(rsmSet);

        return queryArchive(mamQueryIQ);
    }

    private boolean hasMoreMessages(MamQueryIqResult mamQueryIqResult) {
        boolean hasMoreMessages = false;
        int messageCount = mamQueryIqResult.messages != null ? mamQueryIqResult.messages.size() : 0;
        RSMSet rsmSet = mamQueryIqResult.mamFin.getRSMSet();
        if (messageCount < rsmSet.getCount()) {
            hasMoreMessages = true;
        }

        return hasMoreMessages;
    }

    public List<Forwarded> getArchivedMessages(Integer max, Date start, Date end, String withJid) {
        try {
            MamQueryIqResult result = queryArchive(max, start, end, withJid);
            while (hasMoreMessages(result)) {
                MamQueryIqResult nextPage = getNextPage(result, max);
                if (nextPage != null) {
                    result.messages.addAll(nextPage.messages);
                    result.mamFin = nextPage.mamFin;
                } else {
                    break;
                }
            }

            return result.messages;
        } catch (NotConnectedException | XMPPErrorException | NoResponseException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static class MamQueryIqResult {
        private final DataForm form;
        public List<Forwarded> messages;
        public MamFinExtension mamFin;

        private MamQueryIqResult(List<Forwarded> messages, MamFinExtension mamFin, DataForm form) {
            this.messages = messages;
            this.mamFin = mamFin;
            this.form = form;
        }
    }
}