package messagearchive.xep313.provider;

import messagearchive.xep313.Constants;
import messagearchive.xep313.packet.MamPacket.MamResultExtension;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.forward.packet.Forwarded;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class MamResultProvider extends ExtensionElementProvider<MamResultExtension> {

    @Override
    public MamResultExtension parse(XmlPullParser parser, int initialDepth) throws
            XmlPullParserException, IOException, SmackException {
        String id = parser.getAttributeValue("", Constants.ID);
        String queryId = parser.getAttributeValue("", Constants.QUERY_ID);
        Forwarded extension = null;

        boolean done = false;
        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG) {
                String name = parser.getName();
                if (name.equals(Forwarded.ELEMENT)) {
                    extension = (Forwarded) PacketParserUtils.parseExtensionElement(Forwarded
                            .ELEMENT, Forwarded.NAMESPACE, parser);
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getDepth() == initialDepth) {
                    done = true;
                }
            }
        }

        return new MamResultExtension(queryId, id, extension);
    }
}
