package messagearchive.xep313.provider;

import messagearchive.xep313.Constants;
import messagearchive.xep313.packet.MamPacket.MamFinExtension;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.provider.ExtensionElementProvider;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jivesoftware.smackx.rsm.packet.RSMSet;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class MamFinProvider extends ExtensionElementProvider<MamFinExtension> {

    @Override
    public MamFinExtension parse(XmlPullParser parser, int initialDepth) throws
            XmlPullParserException, IOException, SmackException {
        String queryId = parser.getAttributeValue("", Constants.QUERY_ID);
        RSMSet extension = null;

        boolean done = false;
        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG) {
                String name = parser.getName();
                if (name.equals(RSMSet.ELEMENT)) {
                    extension = (RSMSet) PacketParserUtils.parseExtensionElement(RSMSet.ELEMENT,
                            RSMSet.NAMESPACE, parser);
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getDepth() == initialDepth) {
                    done = true;
                }
            }
        }

        return new MamFinExtension(queryId, extension);
    }
}
