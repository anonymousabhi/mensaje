package messagearchive.xep313.filter;

import messagearchive.xep313.packet.MamPacket;
import messagearchive.xep313.packet.MamQueryIQ;

import org.jivesoftware.smack.filter.FlexibleStanzaTypeFilter;
import org.jivesoftware.smack.packet.Message;

public abstract class AbstractMamMessageExtensionFilter extends FlexibleStanzaTypeFilter<Message> {

    private final String queryId;

    public AbstractMamMessageExtensionFilter(MamQueryIQ mamQueryIQ) {
        super(Message.class);
        this.queryId = mamQueryIQ.getQueryId();
    }

    @Override
    protected boolean acceptSpecific(Message message) {
        MamPacket.AbstractMamExtension mamExtension = getMamExtension(message);
        if (mamExtension == null) {
            return false;
        }
        String resultQueryId = mamExtension.getQueryId();
        if (queryId == null && resultQueryId == null) {
            return true;
        } else if (queryId != null && queryId.equals(resultQueryId)) {
            return true;
        }
        return false;
    }

    protected abstract MamPacket.AbstractMamExtension getMamExtension(Message message);
}