package messagearchive.xep313.filter;

import messagearchive.xep313.packet.MamPacket;
import messagearchive.xep313.packet.MamQueryIQ;

import org.jivesoftware.smack.packet.Message;

public class MamMessageFinFilter extends AbstractMamMessageExtensionFilter {

    public MamMessageFinFilter(MamQueryIQ mamQueryIQ) {
        super(mamQueryIQ);
    }

    @Override
    protected MamPacket.AbstractMamExtension getMamExtension(Message message) {
        return MamPacket.MamFinExtension.from(message);
    }

}
