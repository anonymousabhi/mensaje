package messagearchive.xep313;

public class Constants {
    public static final String NAMESPACE = "urn:xmpp:mam:0";

    public static final String ID = "id";
    public static final String QUERY_ID = "queryid";
    public static final String NODE = "node";
    public static final String START = "start";
    public static final String END = "end";
    public static final String WITH = "with";
    public static final String FIN = "fin";
    public static final String RESULT = "result";

    public static final int DEFAULT_PAGE_SIZE = 30;
}
