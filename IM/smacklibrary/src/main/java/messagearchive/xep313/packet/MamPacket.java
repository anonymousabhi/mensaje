package messagearchive.xep313.packet;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.util.StringUtils;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jivesoftware.smackx.forward.packet.Forwarded;
import org.jivesoftware.smackx.rsm.packet.RSMSet;

import messagearchive.xep313.Constants;


/**
 * @see <a href="http://xmpp.org/extensions/xep-0313.html">XEP-0313: Message Archive Management</a>
 */
public class MamPacket {

    public static final String NAMESPACE = Constants.NAMESPACE;

    public static abstract class AbstractMamExtension implements ExtensionElement {
        public final String queryId;

        protected AbstractMamExtension(String queryId) {
            this.queryId = queryId;
        }

        public final String getQueryId() {
            return queryId;
        }


        @Override
        public final String getNamespace() {
            return NAMESPACE;
        }

    }

    public static class MamFinExtension extends AbstractMamExtension {
        public static final String ELEMENT = Constants.FIN;
        private final RSMSet rsmSet;

        public MamFinExtension(String queryId, RSMSet rsmSet) {
            super(queryId);
            if (rsmSet == null) {
                throw new IllegalArgumentException("rsmSet must not be null");
            }

            this.rsmSet = rsmSet;
        }

        public static MamFinExtension from(Message message) {
            return message.getExtension(ELEMENT, NAMESPACE);
        }

        public RSMSet getRSMSet() {
            return rsmSet;
        }

        @Override
        public String getElementName() {
            return ELEMENT;
        }

        @Override
        public XmlStringBuilder toXML() {
            XmlStringBuilder xml = new XmlStringBuilder();
            xml.halfOpenElement(this);
            xml.optAttribute("queryid", queryId);

            if (rsmSet == null) {
                xml.closeEmptyElement();
            } else {
                xml.rightAngleBracket();
                xml.element(rsmSet);
                xml.closeElement(this);
            }

            return xml;
        }
    }

    public static class MamResultExtension extends AbstractMamExtension {

        public static final String ELEMENT = "result";

        private final String id;
        private final Forwarded forwarded;

        public MamResultExtension(String queryId, String id, Forwarded forwarded) {
            super(queryId);
            if (StringUtils.isNullOrEmpty(id)) {
                throw new IllegalArgumentException("id must not be null or empty");
            }
            if (forwarded == null) {
                throw new IllegalArgumentException("forwarded must no be null");
            }
            this.id = id;
            this.forwarded = forwarded;
        }

        public static MamResultExtension from(Message message) {
            return message.getExtension(ELEMENT, NAMESPACE);
        }

        public String getId() {
            return id;
        }

        public Forwarded getForwarded() {
            return forwarded;
        }

        @Override
        public String getElementName() {
            return ELEMENT;
        }

        @Override
        public CharSequence toXML() {
            XmlStringBuilder xml = new XmlStringBuilder();
            xml.halfOpenElement(this);
            xml.optAttribute(Constants.QUERY_ID, queryId);
            xml.rightAngleBracket();
            xml.element(forwarded);
            xml.closeElement(this);
            return xml;
        }

    }
}
