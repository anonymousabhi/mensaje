package messagearchive.xep313.packet;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smackx.xdata.FormField;
import org.jivesoftware.smackx.xdata.packet.DataForm;

import messagearchive.xep313.Constants;

public class MamQueryIQ extends IQ {

    public static final String ELEMENT = QUERY_ELEMENT;
    public static final String NAMESPACE = Constants.NAMESPACE;

    private String queryId;
    private String node;

    private MamQueryIQ(String queryId, String node, DataForm form) {
        super(ELEMENT, NAMESPACE);

        this.queryId = queryId;
        this.node = node;

        if (form != null) {
            FormField field = form.getHiddenFormTypeField();
            if (field == null) {
                throw new IllegalArgumentException("If a data form is given it must posses a " +
                        "hidden form type field");
            }

            if (!field.getValues().get(0).equals(MamPacket.NAMESPACE)) {
                throw new IllegalArgumentException("Value of the hidden form type field must be " +
                        "'" + MamPacket.NAMESPACE + "'");
            }

            addExtension(form);
        }
    }

    public MamQueryIQ(DataForm form) {
        this(null, null, form);
    }

    public MamQueryIQ(String queryId) {
        this(queryId, null, null);
    }

    public MamQueryIQ(String queryId, DataForm form) {
        this(queryId, null, form);
    }

    public String getQueryId() {
        return queryId;
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(
            IQChildElementXmlStringBuilder xml) {
        xml.optAttribute(Constants.QUERY_ID, queryId);
        xml.optAttribute(Constants.NODE, node);
        xml.rightAngleBracket();
        return xml;
    }
}
